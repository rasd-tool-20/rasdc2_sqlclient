create or replace package         RASDC2_SQLCLIENT is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_SQLCLIENT generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  function changes(p_log out varchar2) return varchar2 ;
procedure codemirrorjs(n varchar2, h varchar2, d varchar2, f varchar2);
end;

/
create or replace package body         RASDC2_SQLCLIENT is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_SQLCLIENT generated on 28.02.24 by user RASDDEV.
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);  RECNUMB10                     number := 1
;  RECNUMP                       number := 1
;
  ACTION                        varchar2(4000);  GBUTTONCLR                    varchar2(4000) := 'GBUTTONCLR'
;  PAGE                          number := 0
;
  LANG                          varchar2(4000);
  PFORMID                       varchar2(4000);
  PFORM                         varchar2(4000);  GBUTTONSRC                    varchar2(4000) := RASDI_TRNSLT.text('Search',LANG)
;  GBUTTONSAVE                   varchar2(4000) := RASDI_TRNSLT.text('Save',LANG)
;
  GBUTTONSAVE#SET                set_type;  GBUTTONCOMPILE                varchar2(4000) := RASDI_TRNSLT.text('Compile',LANG)
;
  GBUTTONCOMPILE#SET             set_type;  GBUTTONRES                    varchar2(4000) := RASDI_TRNSLT.text('Reset',LANG)
;
  GBUTTONRES#SET                 set_type;  GBUTTONPREV                   varchar2(4000) := RASDI_TRNSLT.text('Preview',LANG)
;
  GBUTTONPREV#SET                set_type;
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);
  VUSER                         varchar2(4000);
  VLOB                          varchar2(4000);  GBUTTONSUBMIT                 varchar2(4000) := RASDI_TRNSLT.text('Submit',LANG)
;
  HINTCONTENT                   varchar2(4000);
  PSESSSTORAGEENABLED           ctab;
  B10TEXTAREA                   ctab;
  B1OWNER_ID                    ctab;
  B1DML                         ctab;
  B1OWNER                       ctab;
  B1ID                          ctab;
  B30PLSQL                      ctab;
  B30SPOROCILO                  ctab;
  B10SUBMIT                     ctab;
  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := '/* Change LOG:

20230301 - Created new 2 version

20171219 - Changed SQL execution. Now uses local package EXECUTESQL

20171212 - Added SQL logging, ...

20160310 - Included CodeMirror

20141027 - Added footer on all pages

*/';

    return version;

 end;
procedure htpClob(v_clob clob);



function codemirrorrasd(pf varchar2) return clob is

 v_ret clob;

 v_currentDADUser varchar2(50) := rasdc_library.currentDADUser;



begin



v_ret := v_ret  ||' __RASD_VARIABLES: {},';

for r in (  SELECT blockid, fieldid, blockid||fieldid polje

            FROM RASD_FIELDS

           where formid = pf

           order by nvl(blockid,'.'), fieldid) loop

v_ret := v_ret  ||'      "'||r.polje||'": {},';

end loop;

for r in (  SELECT blockid, fieldid, blockid||fieldid||'#SET' polje

            FROM RASD_FIELDS f

           where formid = pf

             and includevis = 'Y'

           order by nvl(blockid,'.'), fieldid) loop

v_ret := v_ret  ||'      "'||r.polje||'.visible":{},';

v_ret := v_ret  ||'      "'||r.polje||'.disabled":{},';

v_ret := v_ret  ||'      "'||r.polje||'.readonly":{},';

v_ret := v_ret  ||'      "'||r.polje||'.required":{},';

v_ret := v_ret  ||'      "'||r.polje||'.error":{},';

v_ret := v_ret  ||'      "'||r.polje||'.info":{},';

v_ret := v_ret  ||'      "'||r.polje||'.custom":{},';

end loop;

v_ret := v_ret  ||'"__RASD_PROCEDURES:" : {},';

v_ret := v_ret  ||'"rlog( message VARCHAR2 );" : {},';

v_ret := v_ret  ||'"htpClob( text CLOB );" : {},';

v_ret := v_ret  ||'"openLOV( p_lov varchar2,  p_value varchar2) return lovtab__ (label VARHAR2,id VARCHAR2)" : {},';

v_ret := v_ret  ||'"version" : {},';

v_ret := v_ret  ||'"this_form" : {},';

v_ret := v_ret  ||'"psubmit(name_array, value_array)" : {},';

v_ret := v_ret  ||'"pclear" : {},';

v_ret := v_ret  ||'"pselect" : {},';

v_ret := v_ret  ||'"pcommit" : {},';

v_ret := v_ret  ||'"poutput" : {},';



for rx in (

  select 1 from rasd_forms f where f.formid = pf

  and f.autocreaterestyn = 'Y'

) loop

v_ret := v_ret  ||'"poutputrest" : {},';

v_ret := v_ret  ||'"poutputrest() return clob" : {},';

end loop;



for r in (select blockid from rasd_blocks b where b.formid = pf order by blockid)

loop

v_ret := v_ret  ||'"pclear_'||r.blockid||'(pstart number)" : {},';

v_ret := v_ret  ||'"pselect_'||r.blockid||'" : {},';

v_ret := v_ret  ||'"pcommit_'||r.blockid||'" : {},';

end loop;



return v_ret;



end;



function codemirrordb(pf varchar2) return clob is

 v_ret clob;

 v_currentDADUser varchar2(50) := rasdc_library.currentDADUser;



function add_attributes(powner varchar2, pname varchar2, pprocedure varchar2) return varchar2 is

   v_ret varchar2(32000);

 begin

for r in (

select x.argument_name, x.data_type from all_arguments x where

  x.owner = powner

  and x.object_name = pprocedure

  and x.package_name = pname

  and x.argument_name is not null

order by sequence

) loop

v_ret := v_ret||','||r.argument_name||' '||r.data_type;

end loop;

if v_ret is null then

   return '';

else

   return '('||substr(v_ret,'2')||')';

end if;

end;



function add_columns (powner varchar2, pname varchar2) return varchar2 is

 v_fir number := 1;

 v_ret varchar2(32000) := '';

 begin



for r in (

select a.owner, a.table_name o_name,a.column_name c_name, a.column_id vr, 'T' vir from all_tab_columns a

where a.owner = powner

  and column_name is not null

  and a.table_name = pname

  and a.column_name not like '%$%'

  and a.column_name not like '%#%'

union

select p.owner, p.object_name, p.procedure_name , p.subprogram_id  , 'P' from all_procedures p

where p.owner = powner

  and procedure_name is not null

  and p.object_name = pname

  and p.procedure_name not like '%$%'

  and p.procedure_name not like '%#%'

order by vr

) loop

if r.vir = 'P' then

  if v_fir = 1 then

v_ret := v_ret ||'       "'||r.c_name||add_attributes(powner, pname,r.c_name)||'" ';

     v_fir := 2;

  else

v_ret := v_ret ||'      , "'||r.c_name||add_attributes(powner, pname,r.c_name)||'" ';

  end if;

else

  if v_fir = 1 then

v_ret := v_ret ||'       "'||r.c_name||'" ';

     v_fir := 2;

  else

v_ret := v_ret ||'      , "'||r.c_name||'" ';

  end if;

end if;

end loop;



return v_ret;

 end;



begin



for r in (

                  select /*+ RULE*/ owner ,OBJECT_NAME id,

                          OBJECT_NAME || ' ... ' || substr(object_type, 1, 1) label,

                          2 x

                    from all_objects

                   where object_type in ('TABLE', 'VIEW')

                     and (owner = v_currentDADUser)

                     and object_name not like '%$%'

                     and object_name not like '%#%'

                  union

                  select /*+ RULE*/ distinct table_owner , SYNONYM_NAME id,

                                   SYNONYM_NAME || ' ... S' label,

                                   2 x

                    from user_synonyms s, all_tab_columns tc

                   where s.table_name = tc.table_name

                     and s.table_owner = tc.owner

                     and s.table_name not like '%$%'

                     and s.table_name not like '%#%'

                   union

                   select distinct owner , table_name id,

                          owner||'.'||table_name  /*|| ' ... ' || substr(type, 1, 1) */ label, 2 x

                   from dba_tab_privs x

                   where

                    grantee = v_currentDADUser

                    and table_name not like '%$%'

                    and table_name not like '%#%'

                   order by  1, 2

) loop

if r.owner = v_currentDADUser then

v_ret := v_ret  ||'      , "'||r.id||'": [';

else

v_ret := v_ret  ||'      , "'||r.owner||'.'||r.id||'": [';

end if;

v_ret := v_ret  || add_columns (r.owner, r.id) ;

v_ret := v_ret  ||'] ';

end loop;



return v_ret;



end;





procedure codemirrorjs(n varchar2, h varchar2, d varchar2, f varchar2) is

  d1 varchar2(100) := d;

  d2 varchar2(100);

begin



if d is null then

d2 := rasdi_client.sessionGetValue('SESSSTORAGEENABLED');

if d2 is null then

rasdi_client.sessionSetValue('SESSSTORAGEENABLED', to_char(sysdate,'ddmmyyyyhh24miss') );

rasdi_client.sessionclose;

else

d1:=d2;

end if;

end if;



htp.p('

function rasd_codemirror_'||n||'() {



var mime = "text/x-sql";

  // get mime type

  if (window.location.href.indexOf("mime=") > -1) {

    mime = window.location.href.substr(window.location.href.indexOf("mime=") + 5);

  }

');





htp.p('var rasdhintoptions;');



if d1 is null then

htp.p('rasdhintoptions = sessionStorage.getItem("rasdi$UserDbHint"); if (rasdhintoptions == null) {rasdhintoptions = '''';} ');

htp.p('if (rasdhintoptions.length == 0) {');

htp.prn('rasdhintoptions = ''');

htpClob(codemirrordb(f));

htp.p(''';');

htp.p('sessionStorage.setItem("rasdi$UserDbHint",rasdhintoptions);');

--htp.p('} else {');

--htp.p('rasdhintoptions = rasdhintoptions; ');

htp.p('}');

htp.p('$("#SESSSTORAGEENABLED_1_RASD").val("'||to_char(sysdate,'ddmmyyyyhh24miss')||'");');

else

htp.p('rasdhintoptions = sessionStorage.getItem("rasdi$UserDbHint"); if (rasdhintoptions == null) {rasdhintoptions = '''';} ');

end if;





htp.prn('var rasdhintoptionsuser = ''');

htp.prn('{tables: {');

htpClob(codemirrorrasd(f));

htp.prn(' __USER_OBJECTS: {}');

htp.prn('''+rasdhintoptions+''');

htp.prn(', __OTHER_FUNCTIONS: {}');

htp.p('    }}'';');





htp.p(' var rasdhintoptionsobj = eval(''(''+rasdhintoptionsuser+'')'');');



htp.p('

if (document.getElementById("'||n||'") != null) {



window.rasd_CMEditor'||n||' = CodeMirror.fromTextArea(document.getElementById("'||n||'"), {

    mode: "sql",

    indentWithTabs: true,

    smartIndent: true,

    lineNumbers: true,

	lineWrapping: true,

    matchBrackets : true,

    autofocus: true,

    foldGutter: true,

	gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],

    foldOptions: {

  rangeFinder: (cm, pos) => {

    var line=window.rasd_CMEditor'||n||'.getLine(pos.line);

	  var match=line.match(/---- /);



    if (match==null) {

      return CodeMirror.fold.auto(cm,pos);

    }

    else {

      var lineend = window.rasd_CMEditor'||n||'.lineCount();

      for (let i = pos.line+1; i < lineend; i++) {

         if ( window.rasd_CMEditor'||n||'.getLine(i).match(/---- /) == null )

            {}

         else

            {lineend = i-1; break; }

      }

        var startPos=CodeMirror.Pos(pos.line+1, 0);

	    var endPos=CodeMirror.Pos(lineend, 0);

      return endPos && CodeMirror.cmpPos(endPos, startPos) > 0 ? {from: startPos, to: endPos} : null;

    }

  }

    },

    extraKeys: {"Ctrl-Space": "autocomplete",

	            "Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); },

	            "Ctrl-Y": cm => CodeMirror.commands.foldAll(cm),

                "Ctrl-I": cm => CodeMirror.commands.unfoldAll(cm),

                "Alt-S": function(instance) { document.'||this_form||'.ACTION.value='''||GBUTTONSAVE||'''; document.'||this_form||'.submit(); },

                "Ctrl-Alt": function(instance) { if ($("#B1_DIV").css("display") ==="block") {$("#B1_DIV").css("display","none")} else { $("#B1_DIV").css("display","block")} },

	            "F11": function(cm) {cm.setOption("fullScreen", !cm.getOption("fullScreen"));},

	            "Esc": function(cm) { if (cm.getOption("fullScreen")) {cm.setOption("fullScreen", false);}  if ($("#B1_DIV").css("display") ==="block") {$("#B1_DIV").css("display","none")} }

	  },

    hintOptions: rasdhintoptionsobj

    });



  //window.rasd_CMEditor'||n||'.foldCode(CodeMirror.Pos(1, 0));

}



}');





end;






function showLabel(plabel varchar2, pcolor varchar2 default 'U', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,' ',''),'.',''),lang,replace(this_form,'2','')||'_DIALOG');

end if;



if pcolor is null then



return v__;



else



return '<font color="'||pcolor||'">'||v__||'</font>';



end if;





end;
procedure post_submit_template is

begin

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text('User has no privileges to save data!', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, 'COMPILE_S', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text('From is not generated.', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           'begin ' || v_server || '.c_debug := false;'|| v_server || '.form(' || PFORMID ||

                           ',''' || lang || ''');end;',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text('From is generated.', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || '<br/> - '||  RASDI_TRNSLT.text('To unlink referenced code check:', lang)||'<input type="checkbox" name="UNLINK" value="Y"/>.';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text('Form is generated with compilation error. Check your code.', lang)||'('||sqlerrm||')';



            else

            sporocilo := RASDI_TRNSLT.text('Form is NOT generated - internal RASD error.', lang) || '('||sqlerrm||')<br>'||

                         RASDI_TRNSLT.text('To debug run: ', lang) || 'begin ' || v_server || '.form(' || PFORMID ||

                         ',''' || lang || ''');end;' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, 'COMPILE_E', pcompid);

end;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop;
       end;
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('RASDC2_SQLCLIENT',v_clob, systimestamp, '' );
       end;
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '
<script type="text/javascript" src="RASDC2_SQLCLIENT.codemirrorjs?n=B10TEXTAREA_1_RASD&h=150&d='||PSESSSTORAGEENABLED(1)||'&f='||pformid||'"  ></script>
';
       end;
     function form_js return clob is
          v_out clob;
       begin
        v_out :=  '
$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)



if ( $("#B10TEXTAREA_1_RASD").val().length > 0 )  {

   $( "#B30_DIV" ).dialog({

          height: "auto",

           width: 1000

    }

    );

}



 });







$(function() {





  rasd_codemirror_B10TEXTAREA_1_RASD();



  window.rasd_CMEditorB10TEXTAREA_1_RASD.setSize("1000","450");

  //32768 max each spec char un UTF8 has 2byts but in editor counts 1byte

  window.rasd_CMEditorB10TEXTAREA_1_RASD.setOption("maxLength", 32000); //32000 is limit of vc_arr type in plsql for length of value  of one element



  window.rasd_CMEditorB10TEXTAREA_1_RASD.on("beforeChange", function (cm, change) {

    var maxLength = cm.getOption("maxLength");

    var isChrome =  window.chrome;

    /*if (maxLength && change.update) {

        var str = change.text.join("\n");

        var delta = str.length-(cm.indexFromPos(change.to) - cm.indexFromPos(change.from));

        var aaa = cm.doc.getValue();

        if (delta <= 0) { return true; }

        xval = cm.getValue()

        if(isChrome){

          xval = xval.replace(/(\r\n|\n|\r)/g,"  ");

        }

        delta = xval.length+delta-maxLength;

        document.getElementById("PLSQLCOUNT").innerHTML = "'|| RASDI_TRNSLT.text('Characters left:', lang) ||' " + ((delta*-1)+1) ;

        if (delta > 0) {

            str = str.substr(0, str.length-delta);

            change.update(change.from, change.to, str.split("\n"));

        }

    }*/

    return true;

  });







});


$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("'|| RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) ||'");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
        ';
        return v_out;
       end;
     function form_css return clob is
          v_out clob;
       begin
        v_out :=  '
.rasdTxPTRIGGERIDPROC INPUT{

   width: 300px;

}



.rasdTxPF INPUT{

   width: 300px;

}





#GBUTTONSAVE_RASD {

    display: none;

}



#GBUTTONCOMPILE_RASD {

    display: none;

}



#GBUTTONPREV_RASD {

     display: none;

}



#GBUTTONSRC_RASD {

    display: none;

}



#B1_DIV{

    vertical-align: top;

    display: none;

    width: 550px;

    height: 400px;

    position: fixed;

    left: 150;

    right: 0;

    top: 200;

    bottom: 0;

    font-size: x-small;

    z-index: auto;

    overflow-y: auto;

        overflow-x: clip;

       border: double;

      background-color: white;

}
        ';
        return v_out;
       end;
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;
  function version return varchar2 is
  begin
   return 'v.1.1.20240228091006';
  end;
  function this_form return varchar2 is
  begin
   return 'RASDC2_SQLCLIENT';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version );
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else
declare vc varchar2(2000); begin
null;
exception when others then  null; end;
  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,RECNUMB10 number PATH '$.recnumb10'
  ,RECNUMP number PATH '$.recnump'
  ,ACTION varchar2(4000) PATH '$.action'
  ,PAGE number PATH '$.page'
  ,LANG varchar2(4000) PATH '$.lang'
  ,PFORMID varchar2(4000) PATH '$.pformid'
  ,GBUTTONSAVE varchar2(4000) PATH '$.gbuttonsave'
  ,GBUTTONCOMPILE varchar2(4000) PATH '$.gbuttoncompile'
  ,GBUTTONRES varchar2(4000) PATH '$.gbuttonres'
  ,GBUTTONPREV varchar2(4000) PATH '$.gbuttonprev'
  ,GBUTTONSUBMIT varchar2(4000) PATH '$.gbuttonsubmit'
  ,HINTCONTENT varchar2(4000) PATH '$.hintcontent'
)) jt ) loop
 if instr(RESTREQUEST,'recnumb10') > 0 then RECNUMB10 := r__.RECNUMB10; end if;
 if instr(RESTREQUEST,'recnump') > 0 then RECNUMP := r__.RECNUMP; end if;
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
 if instr(RESTREQUEST,'lang') > 0 then LANG := r__.LANG; end if;
 if instr(RESTREQUEST,'pformid') > 0 then PFORMID := r__.PFORMID; end if;
 if instr(RESTREQUEST,'gbuttonsave') > 0 then GBUTTONSAVE := r__.GBUTTONSAVE; end if;
 if instr(RESTREQUEST,'gbuttoncompile') > 0 then GBUTTONCOMPILE := r__.GBUTTONCOMPILE; end if;
 if instr(RESTREQUEST,'gbuttonres') > 0 then GBUTTONRES := r__.GBUTTONRES; end if;
 if instr(RESTREQUEST,'gbuttonprev') > 0 then GBUTTONPREV := r__.GBUTTONPREV; end if;
 if instr(RESTREQUEST,'gbuttonsubmit') > 0 then GBUTTONSUBMIT := r__.GBUTTONSUBMIT; end if;
 if instr(RESTREQUEST,'hintcontent') > 0 then HINTCONTENT := r__.HINTCONTENT; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.p' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,PSESSSTORAGEENABLED varchar2(4000) PATH '$.sessstorageenabled'
)) jt ) loop
 if instr(RESTREQUEST,'sessstorageenabled') > 0 then PSESSSTORAGEENABLED(i__) := r__.PSESSSTORAGEENABLED; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B10TEXTAREA varchar2(4000) PATH '$.b10textarea'
  ,B10SUBMIT varchar2(4000) PATH '$.b10submit'
)) jt ) loop
 if instr(RESTREQUEST,'b10textarea') > 0 then B10TEXTAREA(i__) := r__.B10TEXTAREA; end if;
 if instr(RESTREQUEST,'b10submit') > 0 then B10SUBMIT(i__) := r__.B10SUBMIT; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b1[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B1DML varchar2(4000) PATH '$.b1dml'
)) jt ) loop
if r__.x__ is not null then
 if instr(RESTREQUEST,'b1dml') > 0 then B1DML(i__) := r__.B1DML; end if;
i__ := i__ + 1;
end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b30' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B30PLSQL varchar2(4000) PATH '$.b30plsql'
)) jt ) loop
 if instr(RESTREQUEST,'b30plsql') > 0 then B30PLSQL(i__) := r__.B30PLSQL; end if;
i__ := i__ + 1;
end loop;
  end;
  function validate_submit(v_text varchar2) return varchar2 is
    v_outt varchar2(32000) := v_text;
  begin
    if instr(v_outt,'"') > 0 then v_outt := replace(v_outt,'"','&quot;');
    elsif instr(v_outt,'%22') > 0 then v_outt := replace(v_outt,'%22','&quot;');
    elsif instr(lower(v_outt),'<script') > 0 then v_outt := replace(v_outt,'<script','&lt;script');
    end if;
    return v_outt;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB10') then RECNUMB10 := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMP') then RECNUMP := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCLR') then GBUTTONCLR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('LANG') then LANG := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORMID') then PFORMID := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORM') then PFORM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSAVE') then GBUTTONSAVE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCOMPILE') then GBUTTONCOMPILE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONRES') then GBUTTONRES := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONPREV') then GBUTTONPREV := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VUSER') then VUSER := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VLOB') then VLOB := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSUBMIT') then GBUTTONSUBMIT := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('HINTCONTENT') then HINTCONTENT := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PSESSSTORAGEENABLED(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10TEXTAREA_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10TEXTAREA(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B1OWNER_ID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1OWNER_ID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1DML_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1DML(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1OWNER_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1OWNER(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B1ID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B30PLSQL_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B30PLSQL(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B30SPOROCILO_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B30SPOROCILO(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10SUBMIT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10SUBMIT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED') and PSESSSTORAGEENABLED.count = 0 and value_array(i__) is not null then
        PSESSSTORAGEENABLED(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10TEXTAREA') and B10TEXTAREA.count = 0 and value_array(i__) is not null then
        B10TEXTAREA(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B1OWNER_ID') and B1OWNER_ID.count = 0 and value_array(i__) is not null then
        B1OWNER_ID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1DML') and B1DML.count = 0 and value_array(i__) is not null then
        B1DML(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1OWNER') and B1OWNER.count = 0 and value_array(i__) is not null then
        B1OWNER(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B1ID') and B1ID.count = 0 and value_array(i__) is not null then
        B1ID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B30PLSQL') and B30PLSQL.count = 0 and value_array(i__) is not null then
        B30PLSQL(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B30SPOROCILO') and B30SPOROCILO.count = 0 and value_array(i__) is not null then
        B30SPOROCILO(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10SUBMIT') and B10SUBMIT.count = 0 and value_array(i__) is not null then
        B10SUBMIT(1) := validate_submit(value_array(i__));
      end if;
    end loop;
-- organize records
declare
v_last number :=
B1OWNER_ID
.last;
v_curr number :=
B1OWNER_ID
.first;
i__ number;
begin
 if v_last <>
B1OWNER_ID
.count then
   v_curr :=
B1OWNER_ID
.FIRST;
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B1OWNER_ID.exists(v_curr) then B1OWNER_ID(i__) := B1OWNER_ID(v_curr); end if;
      if B1DML.exists(v_curr) then B1DML(i__) := B1DML(v_curr); end if;
      if B1OWNER.exists(v_curr) then B1OWNER(i__) := B1OWNER(v_curr); end if;
      if B1ID.exists(v_curr) then B1ID(i__) := B1ID(v_curr); end if;
      i__ := i__ + 1;
      v_curr :=
B1OWNER_ID
.NEXT(v_curr);
   END LOOP;
      B1OWNER_ID.DELETE(i__ , v_last);
      B1DML.DELETE(i__ , v_last);
      B1OWNER.DELETE(i__ , v_last);
      B1ID.DELETE(i__ , v_last);
end if;
end;
-- init fields
    v_max := 0;
    if B1OWNER_ID.count > v_max then v_max := B1OWNER_ID.count; end if;
    if B1DML.count > v_max then v_max := B1DML.count; end if;
    if B1OWNER.count > v_max then v_max := B1OWNER.count; end if;
    if B1ID.count > v_max then v_max := B1ID.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B1OWNER_ID.exists(i__) then
        B1OWNER_ID(i__) := null;
      end if;
      if not B1DML.exists(i__) then
        B1DML(i__) := null;
      end if;
      if not B1OWNER.exists(i__) then
        B1OWNER(i__) := null;
      end if;
      if not B1ID.exists(i__) then
        B1ID(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if B10TEXTAREA.count > v_max then v_max := B10TEXTAREA.count; end if;
    if B10SUBMIT.count > v_max then v_max := B10SUBMIT.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not B10TEXTAREA.exists(i__) then
        B10TEXTAREA(i__) := null;
      end if;
      if not B10SUBMIT.exists(i__) then
        B10SUBMIT(i__) := gbuttonsubmit;
      end if;
    null; end loop;
    v_max := 0;
    if B30PLSQL.count > v_max then v_max := B30PLSQL.count; end if;
    if B30SPOROCILO.count > v_max then v_max := B30SPOROCILO.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not B30PLSQL.exists(i__) then
        B30PLSQL(i__) := null;
      end if;
      if not B30SPOROCILO.exists(i__) then
        B30SPOROCILO(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if PSESSSTORAGEENABLED.count > v_max then v_max := PSESSSTORAGEENABLED.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not PSESSSTORAGEENABLED.exists(i__) then
        PSESSSTORAGEENABLED(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin
--<POST_SUBMIT formid="101" blockid="">
----put procedure in the begining of trigger;

post_submit_template;



      if B10TEXTAREA(1) is not null then



          if instr(upper(B10TEXTAREA(1)), 'DESCR') = 1 then

            B10TEXTAREA(1) := 'select column_name, data_type,data_length, decode(nullable,''N'',''NOT NULL'','''')

                               from all_tab_cols t

                               where table_name = ''' ||

                               upper(ltrim(rtrim(substr(B10TEXTAREA(1), 6)))) || '''';

          end if;





--check exists of EXECUTESQL

    declare

      n number;

	  nxxx number;

	  verr varchar2(32000);

    begin

        select count(*) into nxxx from all_objects o where o.object_name = 'EXECUTESQL' and o.object_type = 'PACKAGE' and o.owner = user;







        if nxxx = 0 then

            B30SPOROCILO(1) := '<FONT color=red>Package EXECUTESQL missing in your schema ('||user||'). Compile form EXECUTESQL on your schema (select form from suported RASD application form list) and run it to execute grants.</FONT>';

			ERROR := B30SPOROCILO(1);

        else

          execute immediate

'

declare ns number; ni number; nd number; nu number; ve varchar2(1000); vret varchar2(1000);

begin

   :1 := '||user||'.EXECUTESQL.run(:2, :3);

end;

'

using out verr, in B10TEXTAREA(1), out n

;

          if verr is not null then

                        B30SPOROCILO(1) := '<FONT color=red>Logged user: '||user||':' || verr || '</FONT>';

						ERROR := B30SPOROCILO(1);

          else

             if instr(upper(B10TEXTAREA(1)), 'SELECT') > 0 then

                 B30SPOROCILO(1) := 'Only first 100 rows are shown.';

             else

               if instr(upper(B10TEXTAREA(1)), 'UPDATE') > 0 then

                  B30SPOROCILO(1) := '' || n || ' rows were updated.';

               elsif instr(upper(B10TEXTAREA(1)), 'INSERT') > 0 then

                  B30SPOROCILO(1) := '' || n || ' rows were inserted.';

                elsif instr(upper(B10TEXTAREA(1)), 'DELETE') > 0 then

                  B30SPOROCILO(1) := '' || n || ' rows were deleted.';

                else

                  B30SPOROCILO(1) := '' || n || ' rows were efected.';

                end if;

             end if;

          end if;





        end if;



    end;

-----------

        -- IN SQL, LANG

        -- OUT MESSAGE, NUMBER ,

/*

        declare

          cid     pls_integer;

          n       pls_integer;

          ninsert number;

          ndelete number;

          nupdate number;

        begin

          cid := dbms_sql.open_cursor;

          dbms_sql.parse(cid, B10TEXTAREA(1), dbms_sql.native);

          n := dbms_sql.execute(cid);

          if n >= 0 then

            nselect := instr(upper(B10TEXTAREA(1)), 'SELECT');

            ninsert := instr(upper(B10TEXTAREA(1)), 'INSERT');

            nupdate := instr(upper(B10TEXTAREA(1)), 'UPDATE');

            ndelete := instr(upper(B10TEXTAREA(1)), 'DELETE');

            if nselect = 1 then

              -- PLSQL CODE run on UI

              B30SPOROCILO(1) := 'Only first 100 rows are shown.';

            elsif ninsert = 1 then

              B30SPOROCILO(1) := '' || n || ' rows were inserted.';

            elsif ndelete = 1 then

              B30SPOROCILO(1) := '' || n || ' rows were deleted.';

            elsif nupdate = 1 then

              B30SPOROCILO(1) := '' || n || ' rows were updated.';

            end if;

          end if;

        exception

          when others then

            B30SPOROCILO(1) := '<FONT color=red>Logged user: '||user||':' || sqlerrm || '</FONT>';

        end;

*/





      end if;
--</POST_SUBMIT>
    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_P(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        PSESSSTORAGEENABLED(i__) := null;

      end loop;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10TEXTAREA(i__) := null;
        B10SUBMIT(i__) := gbuttonsubmit;

      end loop;
  end;
  procedure pclear_B1(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B1OWNER_ID.delete; end if;
      for i__ in 1..j__ loop
      begin
      B1DML(i__) := B1DML(i__);
      exception when others then
        B1DML(i__) := null;

      end;
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B1OWNER_ID(i__) := null;
        B1DML(i__) := null;
        B1OWNER(i__) := null;
        B1ID(i__) := null;

      end loop;
  end;
  procedure pclear_B30(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B30PLSQL(i__) := null;
        B30SPOROCILO(i__) := null;

      end loop;
  end;
  procedure pclear_form is
  begin
    RECNUMB10 := 1;
    RECNUMP := 1;
    GBUTTONCLR := 'GBUTTONCLR';
    PAGE := 0;
    LANG := null;
    PFORMID := null;
    PFORM := null;
    GBUTTONSRC := RASDI_TRNSLT.text('Search',LANG);
    GBUTTONSAVE := RASDI_TRNSLT.text('Save',LANG);
    GBUTTONCOMPILE := RASDI_TRNSLT.text('Compile',LANG);
    GBUTTONRES := RASDI_TRNSLT.text('Reset',LANG);
    GBUTTONPREV := RASDI_TRNSLT.text('Preview',LANG);
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
    VUSER := null;
    VLOB := null;
    GBUTTONSUBMIT := RASDI_TRNSLT.text('Submit',LANG);
    HINTCONTENT := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_P(0);
    pclear_B10(0);
    pclear_B1(0);
    pclear_B30(0);

  null;
  end;
  procedure pselect_P is
    i__ pls_integer;
  begin
      pclear_P(PSESSSTORAGEENABLED.count);
  null; end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      pclear_B10(B10TEXTAREA.count);
  null; end;
  procedure pselect_B1 is
    i__ pls_integer;
  begin
      B1OWNER_ID.delete;
      B1OWNER.delete;
      B1ID.delete;

      declare
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR
--<SQL formid="101" blockid="B1">
SELECT
OWNER_ID,
OWNER,
ID from(

  select /*+ RULE*/ owner ,OBJECT_NAME id,

                    owner||'.'||object_name owner_id,

                          OBJECT_NAME || ' ... ' || substr(object_type, 1, 1) label,

                          2 x

                    from all_objects

                   where object_type in ('TABLE', 'VIEW')

                     and (owner = rasdc_library.currentDADUser)

                  union

                  select /*+ RULE*/ distinct table_owner , SYNONYM_NAME id,

  table_owner||'.'||synonym_name owner_id,

                                   SYNONYM_NAME || ' ... S' label,

                                   2 x

                    from user_synonyms s, all_tab_columns tc

                   where s.table_name = tc.table_name

                     and s.table_owner = tc.owner

                     --and (s.owner = rasdc_library.currentDADUser)

                   union

                   select distinct owner , table_name id,

  owner||'.'||table_name owner_id,

                          owner||'.'||table_name  /*|| ' ... ' || substr(type, 1, 1) */ label, 2 x

                   from dba_tab_privs x

                   where --type in ('TABLE', 'VIEW') and

                    grantee = rasdc_library.currentDADUser

)

order by 1, 2
--</SQL>
;
        i__ := 1;
        LOOP
          FETCH c__ INTO
            B1OWNER_ID(i__)
           ,B1OWNER(i__)
           ,B1ID(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B1OWNER_ID.delete(1);
          B1DML.delete(1);
          B1OWNER.delete(1);
          B1ID.delete(1);
          i__ := 0;
        end if;
        CLOSE c__;
      end;
      pclear_B1(B1OWNER_ID.count);
  null; end;
  procedure pselect_B30 is
    i__ pls_integer;
  begin
      pclear_B30(B30PLSQL.count);
  null; end;
  procedure pselect is
  begin

    if  nvl(PAGE,0) = 0 then
      pselect_B1;
    end if;

  null;
 end;
  procedure pcommit_P is
  begin
    for i__ in 1..PSESSSTORAGEENABLED.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10TEXTAREA.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit_B1 is
  begin
    for i__ in 1..B1OWNER_ID.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit_B30 is
  begin
    for i__ in 1..B30PLSQL.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit is
  begin

    if  nvl(PAGE,0) = 0 then
       pcommit_B1;
    end if;

  null;
  end;
  procedure formgen_js is
  begin
    htp.p('function cMFP() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB1() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB30() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
    iB1 pls_integer;
  function ShowFieldERROR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCLR return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONCOMPILE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONPREV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONRES return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSAVE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSRC return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldGBUTTONSUBMIT return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldHINTCONTENT return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldMESSAGE return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldPFORM return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVLOB return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldVUSER return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowFieldWARNING return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB1_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB10_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB30_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockP_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
procedure output_B1_DIV is begin htp.p('');  if  ShowBlockB1_DIV  then
htp.prn('<div  id="B1_DIV" class="rasdblock"><div>
<caption><div id="B1_LAB" class="labelblock"></div></caption><table border="1" id="B1_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB1" id="rasdTxLabB1OWNER_ID"><span id="B1OWNER_ID_LAB" class="label"></span></td><td class="rasdTxLab rasdTxLabBlockB1" id="rasdTxLabB1DML"><span id="B1DML_LAB" class="label"></span></td></tr></thead>'); for iB1 in 1..B1OWNER_ID.count loop
htp.prn('<tr id="B1_BLOCK_'||iB1||'"><td class="rasdTxB1OWNER_ID rasdTxTypeC" id="rasdTxB1OWNER_ID_'||iB1||'"><font id="B1OWNER_ID_'||iB1||'_RASD" class="rasdFont">'||B1OWNER_ID(iB1)||'</font></td><td class="rasdTxB1DML rasdTxTypeC" id="rasdTxB1DML_'||iB1||'"><span id="B1DML_'||iB1||'_RASD">');  htp.p('<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''SELECT \n');

for r_1_ in (

select * from all_tab_columns x where x.owner = B1OWNER(iB1) and x.table_name =  B1ID(iB1)

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p(' '||r_1_.column_name||'\n');

  else

htp.p(','||r_1_.column_name||'\n');

  end if;



end loop;



htp.p('FROM '||B1OWNER(iB1)||'.'||B1ID(iB1)||' \nWHERE 1=2 \nORDER BY 1'') ;">S</A>');





htp.p('<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''INSERT INTO '||B1OWNER(iB1)||'.'||B1ID(iB1)||' \n');

htp.p('(\n');

for r_1_ in (

select * from all_tab_columns x where x.owner = B1OWNER(iB1) and x.table_name =  B1ID(iB1)

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p(' '||r_1_.column_name||'\n');

  else

htp.p(','||r_1_.column_name||'\n');

  end if;

end loop;

htp.p(') VALUES (\n');

for r_1_ in (

select * from all_tab_columns x where x.owner = B1OWNER(iB1) and x.table_name =  B1ID(iB1)

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p(' '||r_1_.column_name||'\n');

  else

htp.p(','||r_1_.column_name||'\n');

  end if;

end loop;

htp.p(')\n'') ;">I</A>');





htp.p('<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''UPDATE '||B1OWNER(iB1)||'.'||B1ID(iB1)||' SET\n');

for r_1_ in (

select * from all_tab_columns x where x.owner = B1OWNER(iB1) and x.table_name =  B1ID(iB1)

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p(' '||r_1_.column_name||' = '||r_1_.column_name||'\n');

  else

htp.p(','||r_1_.column_name||' ='||r_1_.column_name||'\n');

  end if;



end loop;



htp.p('WHERE 1=2 '') ;">U</A>');





htp.p('<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''DELETE '||B1OWNER(iB1)||'.'||B1ID(iB1)||' \n');

htp.p('WHERE 1=2 '') ;">D</A>');



htp.p('</BR>');
htp.prn('</span></td></tr>'); end loop;
htp.prn('</table></div></div>');  end if;
htp.prn(''); end;
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock"></div></caption>
<table border="0" id="B10_TABLE"><tr id="B10_BLOCK_1"><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10TEXTAREA"><span id="B10TEXTAREA_LAB" class="label"></span></td><td class="rasdTxB10TEXTAREA rasdTxTypeC" id="rasdTxB10TEXTAREA_1"><textarea name="B10TEXTAREA_1" id="B10TEXTAREA_1_RASD" class="rasdTextarea ">'||B10TEXTAREA(1)||'</textarea></td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10SUBMIT"><span id="B10SUBMIT_LAB" class="label"></span></td><td class="rasdTxB10SUBMIT rasdTxTypeC" id="rasdTxB10SUBMIT_1"><input onclick=" ACTION.value=this.value; submit();" name="B10SUBMIT_1" id="B10SUBMIT_1_RASD" type="button" value="'||B10SUBMIT(1)||'" class="rasdButton"/>
</td></tr><tr></tr></table></div></div>');  end if;
htp.prn(''); end;
procedure post_output_B10_DIV is  begin
--<post_ui formid="101" blockid="B10">
htp.p('<div class="label">');

htp.p(RASDI_TRNSLT.text('Press CTRL+SPACE to open variable list, CTRL+ALT to get DML code, CTRL+F to search, CTRL-I fold open, CTRL-Y fold close,  ALT+G jump to line, F11 to maximize window. (fold string ----)',lang));

htp.p('<a onclick="$(''#SESSSTORAGEENABLED_1_RASD'').val(''''); document.cookie=''rasdi$SESSSTORAGEENABLED='';">'||RASDI_TRNSLT.text('Click here to refresh DB cache in editor',lang)||'</a>');

htp.p('</div>');


--</post_ui>
  end;
procedure output_B30_DIV is begin htp.p('');  if  ShowBlockB30_DIV  then
htp.prn('<div  id="B30_DIV" class="rasdblock"><div>
<caption><div id="B30_LAB" class="labelblock"></div></caption>
<table border="0" id="B30_TABLE"><tr id="B30_BLOCK_1"><td class="rasdTxLab rasdTxLabBlockB30" id="rasdTxLabB30PLSQL"><span id="B30PLSQL_LAB" class="label"></span></td><td class="rasdTxB30PLSQL rasdTxTypeC" id="rasdTxB30PLSQL_1"><span id="B30PLSQL_1_RASD">');  if length(B10TEXTAREA(1)) > 0 and error is null then

/*

htp.p('<div id="sqlresults_form" title="Results">

<table border="0" id="B30_TABLE">

<tr>

<td>*/

htp.p('<PLSQL ID="B30RESULT_1" CLASS="FONT">');



     if instr(upper(b10textarea(1)), 'SELECT') > 0 then



         execute immediate 'begin  '||user||'.EXECUTESQL.output(:sql,:lang); end;'

         using in b10textarea(1), in lang

         ;



/*

        declare

          cid1 pls_integer;

        begin

          cid1 := owa_util.bind_variables(b10textarea(1));



          htp.p('<div class="sqlresults"><table>');

          owa_util.cellsprint(cid1, 100);

          htp.p('</table></div>');



          b30sporocilo(1) := 'Only first 100 rows are shown.';

       end;

*/

    end if;

      htp.prn('</PLSQL>');

/*	  </td></tr><tr>

<td></br><FONT ID="B30SPOROCILO_1" CLASS="FONT">' ||

              B30SPOROCILO(1) || '</FONT></td>

</tr></table>

</div>');*/



end if;
htp.prn('</span></td></tr><tr><td class="rasdTxLab rasdTxLabBlockB30" id="rasdTxLabB30SPOROCILO"><span id="B30SPOROCILO_LAB" class="label"></span></td><td class="rasdTxB30SPOROCILO rasdTxTypeC" id="rasdTxB30SPOROCILO_1"><font id="B30SPOROCILO_1_RASD" class="rasdFont">'||B30SPOROCILO(1)||'</font></td></tr><tr></tr></table></div></div>');  end if;
htp.prn(''); end;
procedure output_P_DIV is begin htp.p('');  if  ShowBlockP_DIV  then
htp.prn('<div  id="P_DIV" class="rasdblock"><div>
<caption><div id="P_LAB" class="labelblock"></div></caption>
<table border="0" id="P_TABLE"><tr id="P_BLOCK_1"><span id="" value="1" name="" class="hiddenRowItems"><input name="SESSSTORAGEENABLED_1" id="SESSSTORAGEENABLED_1_RASD" type="hidden" value="'||PSESSSTORAGEENABLED(1)||'"/>
</span></tr></table></div></div>');  end if;
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');

htp.prn('</head>
<body><div id="RASDC2_SQLCLIENT_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_SQLCLIENT_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div id="RASDC2_SQLCLIENT_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('RASDC2_SQLCLIENT_MENU') ||'     </div>
<form name="RASDC2_SQLCLIENT" method="post" action="?"><div id="RASDC2_SQLCLIENT_DIV" class="rasdForm"><div id="RASDC2_SQLCLIENT_HEAD" class="rasdFormHead"><input name="RECNUMB10" id="RECNUMB10_RASD" type="hidden" value="'||ltrim(to_char(RECNUMB10))||'"/>
<input name="RECNUMP" id="RECNUMP_RASD" type="hidden" value="'||ltrim(to_char(RECNUMP))||'"/>
<input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
<input name="PAGE" id="PAGE_RASD" type="hidden" value="'||ltrim(to_char(PAGE))||'"/>
<input name="LANG" id="LANG_RASD" type="hidden" value="'||LANG||'"/>
<input name="PFORMID" id="PFORMID_RASD" type="hidden" value="'||PFORMID||'"/>
');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldGBUTTONSUBMIT  then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSUBMIT" id="GBUTTONSUBMIT_RASD" type="button" value="'||GBUTTONSUBMIT||'" class="rasdButton"/>');  end if;
htp.prn('
</div><div id="RASDC2_SQLCLIENT_RESPONSE" class="rasdFormResponse"><div id="RASDC2_SQLCLIENT_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="RASDC2_SQLCLIENT_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="RASDC2_SQLCLIENT_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div></div><div id="RASDC2_SQLCLIENT_BODY" class="rasdFormBody">'); output_P_DIV; htp.p(''); output_B10_DIV; post_output_B10_DIV; htp.p(''); output_B1_DIV; htp.p(''); output_B30_DIV; htp.p('</div><div id="RASDC2_SQLCLIENT_FOOTER" class="rasdFormFooter">');
if  ShowFieldGBUTTONSAVE  then
htp.prn('');  if GBUTTONSAVE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('');  if GBUTTONCOMPILE#SET.visible then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'"
 class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONRES  then
htp.prn('');  if GBUTTONRES#SET.visible then
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;
htp.prn('/>');  end if;
htp.prn('
');  end if;
htp.prn('');
if  ShowFieldGBUTTONPREV  then
htp.prn('');  if GBUTTONPREV#SET.visible then
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;
htp.prn('>');  end if;
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||

                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||

                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

									 ,

                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '

                                )

			       ) || '>'

);
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldHINTCONTENT  then
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('<div style="display: none;">');

htp.p(rasdc_hints.getHint(replace(this_form,'2','')||'_DIALOG',lang));

htp.p('</div>');
htp.prn('</span>');  end if;
htp.prn('');
if  ShowFieldGBUTTONSUBMIT  then
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSUBMIT" id="GBUTTONSUBMIT_RASD" type="button" value="'||GBUTTONSUBMIT||'" class="rasdButton"/>');  end if;
htp.prn('
</div></div></form><div id="RASDC2_SQLCLIENT_BOTTOM" class="rasdFormBottom">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_SQLCLIENT_BOTTOM',1,instr('RASDC2_SQLCLIENT_BOTTOM', '_',-1)-1) , '') ||'</div></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB1_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB10_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockB30_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  function ShowBlockP_DIV return boolean is
  begin
    if  nvl(PAGE,0) = 0 then
       return true;
    end if;
    return false;
  end;
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>');
    htpp('<form name="RASDC2_SQLCLIENT" version="'||version||'">');
    htpp('<formfields>');
    htpp('<recnumb10>'||RECNUMB10||'</recnumb10>');
    htpp('<recnump>'||RECNUMP||'</recnump>');
    htpp('<action><![CDATA['||ACTION||']]></action>');
    htpp('<page>'||PAGE||'</page>');
    htpp('<lang><![CDATA['||LANG||']]></lang>');
    htpp('<pformid><![CDATA['||PFORMID||']]></pformid>');
    htpp('<gbuttonsave><![CDATA['||GBUTTONSAVE||']]></gbuttonsave>');
    htpp('<gbuttoncompile><![CDATA['||GBUTTONCOMPILE||']]></gbuttoncompile>');
    htpp('<gbuttonres><![CDATA['||GBUTTONRES||']]></gbuttonres>');
    htpp('<gbuttonprev><![CDATA['||GBUTTONPREV||']]></gbuttonprev>');
    htpp('<error><![CDATA['||ERROR||']]></error>');
    htpp('<message><![CDATA['||MESSAGE||']]></message>');
    htpp('<warning><![CDATA['||WARNING||']]></warning>');
    htpp('<gbuttonsubmit><![CDATA['||GBUTTONSUBMIT||']]></gbuttonsubmit>');
    htpp('<hintcontent><![CDATA['||HINTCONTENT||']]></hintcontent>');
    htpp('</formfields>');
    if ShowBlockp_DIV then
    htpp('<p>');
    htpp('<element>');
    htpp('<sessstorageenabled><![CDATA['||PSESSSTORAGEENABLED(1)||']]></sessstorageenabled>');
    htpp('</element>');
  htpp('</p>');
  end if;
    if ShowBlockb10_DIV then
    htpp('<b10>');
    htpp('<element>');
    htpp('<b10textarea><![CDATA['||B10TEXTAREA(1)||']]></b10textarea>');
    htpp('<b10submit><![CDATA['||B10SUBMIT(1)||']]></b10submit>');
    htpp('</element>');
  htpp('</b10>');
  end if;
    if ShowBlockb1_DIV then
    htpp('<b1>');
  for i__ in 1..
B1OWNER_ID
.count loop
    htpp('<element>');
    htpp('<b1owner_id><![CDATA['||B1OWNER_ID(i__)||']]></b1owner_id>');
    htpp('<b1dml><![CDATA['||B1DML(i__)||']]></b1dml>');
    htpp('</element>');
  end loop;
  htpp('</b1>');
  end if;
    if ShowBlockb30_DIV then
    htpp('<b30>');
    htpp('<element>');
    htpp('<b30plsql><![CDATA['||B30PLSQL(1)||']]></b30plsql>');
    htpp('<b30sporocilo><![CDATA['||B30SPOROCILO(1)||']]></b30sporocilo>');
    htpp('</element>');
  htpp('</b30>');
  end if;
    htpp('</form>');
else
    htpp('{"form":{"@name":"RASDC2_SQLCLIENT","@version":"'||version||'",' );
    htpp('"formfields": {');
    htpp('"recnumb10":"'||RECNUMB10||'"');
    htpp(',"recnump":"'||RECNUMP||'"');
    htpp(',"action":"'||escapeRest(ACTION)||'"');
    htpp(',"page":"'||PAGE||'"');
    htpp(',"lang":"'||escapeRest(LANG)||'"');
    htpp(',"pformid":"'||escapeRest(PFORMID)||'"');
    htpp(',"gbuttonsave":"'||escapeRest(GBUTTONSAVE)||'"');
    htpp(',"gbuttoncompile":"'||escapeRest(GBUTTONCOMPILE)||'"');
    htpp(',"gbuttonres":"'||escapeRest(GBUTTONRES)||'"');
    htpp(',"gbuttonprev":"'||escapeRest(GBUTTONPREV)||'"');
    htpp(',"error":"'||escapeRest(ERROR)||'"');
    htpp(',"message":"'||escapeRest(MESSAGE)||'"');
    htpp(',"warning":"'||escapeRest(WARNING)||'"');
    htpp(',"gbuttonsubmit":"'||escapeRest(GBUTTONSUBMIT)||'"');
    htpp(',"hintcontent":"'||escapeRest(HINTCONTENT)||'"');
    htpp('},');
    if ShowBlockp_DIV then
    htpp('"p":[');
     htpp('{');
    htpp('"sessstorageenabled":"'||escapeRest(PSESSSTORAGEENABLED(1))||'"');
    htpp('}');
    htpp(']');
  else
    htpp('"p":[]');
  end if;
    if ShowBlockb10_DIV then
    htpp(',"b10":[');
     htpp('{');
    htpp('"b10textarea":"'||escapeRest(B10TEXTAREA(1))||'"');
    htpp(',"b10submit":"'||escapeRest(B10SUBMIT(1))||'"');
    htpp('}');
    htpp(']');
  else
    htpp(',"b10":[]');
  end if;
    if ShowBlockb1_DIV then
    htpp(',"b1":[');
  v_firstrow__ := true;
  for i__ in 1..
B1OWNER_ID
.count loop
    if v_firstrow__ then
     htpp('{');
     v_firstrow__ := false;
    else
     htpp(',{');
    end if;
    htpp('"b1owner_id":"'||escapeRest(B1OWNER_ID(i__))||'"');
    htpp(',"b1dml":"'||escapeRest(B1DML(i__))||'"');
    htpp('}');
  end loop;
    htpp(']');
  else
    htpp(',"b1":[]');
  end if;
    if ShowBlockb30_DIV then
    htpp(',"b30":[');
     htpp('{');
    htpp('"b30plsql":"'||escapeRest(B30PLSQL(1))||'"');
    htpp(',"b30sporocilo":"'||escapeRest(B30SPOROCILO(1))||'"');
    htpp('}');
    htpp(']');
  else
    htpp(',"b30":[]');
  end if;
    htpp('}}');
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_SQLCLIENT',ACTION);
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
  end if;

--<POST_ACTION formid="101" blockid="">


if action = GBUTTONSUBMIT then



pselect;

poutput;



end if;
--</POST_ACTION>
    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||''));
htp.p('');
htp.p('<script type="text/javascript">');
formgen_js;
htp.p('</script>');
htpClob(FORM_UIHEAD);
htp.p('<style type="text/css">');
htpClob(FORM_CSS);
htp.p('</style><script type="text/javascript">');  htp.p('</script>');

htp.prn('</head><body><div id="RASDC2_SQLCLIENT_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_SQLCLIENT_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'RASDC2_SQLCLIENT' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_SQLCLIENT_FOOTER',1,instr('RASDC2_SQLCLIENT_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end;
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_SQLCLIENT',ACTION);
  if ACTION = GBUTTONSAVE then     pselect;
    pcommit;
  end if;

--<POST_ACTION formid="101" blockid="">


if action = GBUTTONSUBMIT then



pselect;

poutput;



end if;
--</POST_ACTION>
-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end;
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin
  rasd_client.secCheckCredentials(  name_array , value_array );

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_SQLCLIENT',ACTION);
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutputrest;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutputrest;
  end if;

--<POST_ACTION formid="101" blockid="">


if action = GBUTTONSUBMIT then



pselect;

poutput;



end if;
--</POST_ACTION>
-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="RASDC2_SQLCLIENT" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;
    htp.p('{"form":{"@name":"RASDC2_SQLCLIENT","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end;
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>101</formid><form>RASDC2_SQLCLIENT</form><version>1</version><change>28.02.2024 09/10/07</change><user>RASDDEV</user><label><![CDATA[<%= rasdc_library.formName(PFORMID, LANG) %>]]></label><lobid>RASDDEV</lobid><program>?</program><referenceyn>Y</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>Y</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>25.08.2023 12/38/22</change><compileyn>Y</compileyn><application>RASD 2.0</application><owner>domen</owner><editor>domen</editor></info></compiledInfo><blocks><block><blockid>B1</blockid><sqltable></sqltable><numrows>0</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[from(

  select /*+ RULE*/ owner ,OBJECT_NAME id,

                    owner||''.''||object_name owner_id,

                          OBJECT_NAME || '' ... '' || substr(object_type, 1, 1) label,

                          2 x

                    from all_objects

                   where object_type in (''TABLE'', ''VIEW'')

                     and (owner = rasdc_library.currentDADUser)

                  union

                  select /*+ RULE*/ distinct table_owner , SYNONYM_NAME id,

  table_owner||''.''||synonym_name owner_id,

                                   SYNONYM_NAME || '' ... S'' label,

                                   2 x

                    from user_synonyms s, all_tab_columns tc

                   where s.table_name = tc.table_name

                     and s.table_owner = tc.owner

                     --and (s.owner = rasdc_library.currentDADUser)

                   union

                   select distinct owner , table_name id,

  owner||''.''||table_name owner_id,

                          owner||''.''||table_name  /*|| '' ... '' || substr(type, 1, 1) */ label, 2 x

                   from dba_tab_privs x

                   where --type in (''TABLE'', ''VIEW'') and

                    grantee = rasdc_library.currentDADUser

)

order by 1, 2  ]]></sqltext><label></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><fie';
 v_vc(2) := 'ld><blockid>B1</blockid><fieldid>DML</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>30</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1DML</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>ID</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>50</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B1ID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>OWNER</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>40</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B1OWNER</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B1</blockid><fieldid>OWNER_ID</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>20</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B1OWNER_ID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>B10</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>Y</clearyn><sqltext><![CDATA[v]]';
 v_vc(3) := '></sqltext><label></label><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B10</blockid><fieldid>SUBMIT</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[gbuttonsubmit]]></defaultval><elementyn>Y</elementyn><nameid>B10SUBMIT</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>TEXTAREA</fieldid><type>C</type><format></format><element>TEXTAREA_</element><hiddenyn></hiddenyn><orderby>10</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10TEXTAREA</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>B30</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B30</blockid><fieldid>PLSQL</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>1000</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B30PLSQL</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B30</blockid><fieldid>SPOROCILO</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>1001</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><d';
 v_vc(4) := 'eleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B30SPOROCILO</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>P</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>P</blockid><fieldid>SESSSTORAGEENABLED</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>SESSSTORAGEENABLED</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block></blocks><fields><field><blockid></blockid><fieldid>ACTION</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ACTION</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ACTION</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>ERROR</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>50</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ERROR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ERROR</rfieldid><includevis>N<';
 v_vc(5) := '/includevis></field><field><blockid></blockid><fieldid>GBUTTONCLR</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''GBUTTONCLR'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONCLR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCLR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>13</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Compile'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONCOMPILE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCOMPILE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONPREV</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>15</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Preview'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONPREV</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONPREV</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONRES</fieldid><type>C</type><format></format><element>INPUT_RESET</element><hiddenyn></hiddenyn><orderby>14</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Reset'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONRES</nameid><label></label><linkid></l';
 v_vc(6) := 'inkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONRES</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>12</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Save'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONSAVE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSAVE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>9</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Search'',LANG)]]></defaultval><elementyn>N</elementyn><nameid>GBUTTONSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSRC</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONSUBMIT</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Submit'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONSUBMIT</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>HINTCONTENT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><';
 v_vc(7) := '/defaultval><elementyn>Y</elementyn><nameid>HINTCONTENT</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>HINTCONTENT</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>LANG</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>5</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>LANG</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>LANG</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>MESSAGE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>51</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>MESSAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>MESSAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PAGE</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[0]]></defaultval><elementyn>Y</elementyn><nameid>PAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>7</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PFORM</nameid><label></label><lin';
 v_vc(8) := 'kid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORM</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORMID</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>6</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFORMID</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORMID</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMB10</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-10</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMB10</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMP</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-2</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMP</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VLOB</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>81</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VLOB</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblocki';
 v_vc(9) := 'd><rfieldid>VLOB</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VUSER</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>80</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VUSER</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VUSER</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>WARNING</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>52</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>WARNING</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>WARNING</rfieldid><includevis>N</includevis></field></fields><links></links><pages><pagedata><page>0</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><';
 v_vc(10) := 'rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBTNNAVIG</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBTNNAVIG</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBTNNAVIG</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBTNNEW</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBTNNEW</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBTNNEW</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><p';
 v_vc(11) := 'agedata><page>2</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></bl';
 v_vc(12) := 'ockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid';
 v_vc(13) := '><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>PF</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata></pages><triggers><trigger><blockid></blockid><triggerid>FORM_CSS</triggerid><plsql><![CDATA[.rasdTxPTRIGGERIDPROC INPUT{

   width: 300px;

}



.rasdTxPF INPUT{

   width: 300px;

}





#GBUTTONSAVE_RASD {

    display: none;

}



#GBUTTONCOMPILE_RASD {

    display: none;

}



#GBUTTONPREV_RASD {

     display: none;

}



#GBUTTONSRC_RASD {

    display: none;

}



#B1_DIV{

    vertical-align: top;

    display: none;

    width: 550px;

    height: 400px;

    position: fixed;

    left: 150;

    right: 0;

    top: 200;

    bottom: 0;

    font-size: x-small;

    z-index: auto;

    overflow-y: auto;

        overflow-x: clip;

       border: double;

      background-color: white;

}
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS</triggerid><plsql><![CDATA[$(function() {



//  addSpinner();

//   initRowStatus();

//   transformVerticalTable("B15_TABLE", 4 );

//   CheckFieldValue(pid , pname)

//   CheckFieldMandatory(pid , pname)



if ( $("#B10TEXTAREA_1_RASD").val().length > 0 )  {

   $( "#B30_DIV" ).dialog({

          height: "auto",

           width: 1000

    }

    );

}



 });







$(function() {





  rasd_codemirror_B10TEXTAREA_1_RASD();



  window.rasd_CMEditorB10TEXTAREA_1_RASD.setSize("1000","450");

  //32768 max each spec char un UTF8 has 2byts but in editor counts 1byte

  window.rasd_CMEditorB10TEXTAREA_1_RASD.setOption("maxLength", 32000); //32000 is limit of vc_arr type in plsql for length of value  of one element



  window.rasd_CMEditorB10TEXTAREA_1_RASD.on("beforeChange", function (cm, change) {

    var maxLength = cm.getOption("maxLength");

    var isChrome =  window.chrome;

    /*if (maxLength && change.update) {

        var str = change.text.join("\n");

        var delta = str.length-(cm.indexFromPos(change.to) - cm.indexFromPos(change.from));

        var aaa = cm.doc.getValue();

        if (delta <= 0) { return true; }

        xval = cm.getValue()

        if(isChrome){

          xval = xval.replace(/(\r\n|\n|\r)/g,"  ");

        }

        delta = xval';
 v_vc(14) := '.length+delta-maxLength;

        document.getElementById("PLSQLCOUNT").innerHTML = "<%= RASDI_TRNSLT.text(''Characters left:'', lang) %> " + ((delta*-1)+1) ;

        if (delta > 0) {

            str = str.substr(0, str.length-delta);

            change.update(change.from, change.to, str.split("\n"));

        }

    }*/

    return true;

  });







});


]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS_REF(82)</triggerid><plsql><![CDATA[$(function() {



  addSpinner();



});



$(function() {



  $(".rasdFormMenu").html("<%= RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) %>");





  $(document).ready(function () {

   $(".dialog").dialog({ autoOpen: false });

  });





});
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_UIHEAD</triggerid><plsql><![CDATA[<script type="text/javascript" src="RASDC2_SQLCLIENT.codemirrorjs?n=B10TEXTAREA_1_RASD&h=150&d=<%=PSESSSTORAGEENABLED(1)%>&f=<%=pformid%>"  ></script>
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B1DML</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p(''<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''''SELECT \n'');

for r_1_ in (

select * from all_tab_columns x where x.owner = b1owner and x.table_name =  b1id

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p('' ''||r_1_.column_name||''\n'');

  else

htp.p('',''||r_1_.column_name||''\n'');

  end if;



end loop;



htp.p(''FROM ''||b1owner||''.''||b1id||'' \nWHERE 1=2 \nORDER BY 1'''') ;">S</A>'');





htp.p(''<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''''INSERT INTO ''||b1owner||''.''||b1id||'' \n'');

htp.p(''(\n'');

for r_1_ in (

select * from all_tab_columns x where x.owner = b1owner and x.table_name =  b1id

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p('' ''||r_1_.column_name||''\n'');

  else

htp.p('',''||r_1_.column_name||''\n'');

  end if;

end loop;

htp.p('') VALUES (\n'');

for r_1_ in (

select * f';
 v_vc(15) := 'rom all_tab_columns x where x.owner = b1owner and x.table_name =  b1id

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p('' ''||r_1_.column_name||''\n'');

  else

htp.p('',''||r_1_.column_name||''\n'');

  end if;

end loop;

htp.p('')\n'''') ;">I</A>'');





htp.p(''<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''''UPDATE ''||b1owner||''.''||b1id||'' SET\n'');

for r_1_ in (

select * from all_tab_columns x where x.owner = b1owner and x.table_name =  b1id

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p('' ''||r_1_.column_name||'' = ''||r_1_.column_name||''\n'');

  else

htp.p('',''||r_1_.column_name||'' =''||r_1_.column_name||''\n'');

  end if;



end loop;



htp.p(''WHERE 1=2 '''') ;">U</A>'');





htp.p(''<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''''DELETE ''||b1owner||''.''||b1id||'' \n'');

htp.p(''WHERE 1=2 '''') ;">D</A>'');



htp.p(''</BR>'');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B30PLSQL</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[if length(b10textarea(1)) > 0 and error is null then

/*

htp.p(''<div id="sqlresults_form" title="Results">

<table border="0" id="B30_TABLE">

<tr>

<td>*/

htp.p(''<PLSQL ID="B30RESULT_1" CLASS="FONT">'');



     if instr(upper(b10textarea(1)), ''SELECT'') > 0 then



         execute immediate ''begin  ''||user||''.EXECUTESQL.output(:sql,:lang); end;''

         using in b10textarea(1), in lang

         ;



/*

        declare

          cid1 pls_integer;

        begin

          cid1 := owa_util.bind_variables(b10textarea(1));



          htp.p(''<div class="sqlresults"><table>'');

          owa_util.cellsprint(cid1, 100);

          htp.p(''</table></div>'');



          b30sporocilo(1) := ''Only first 100 rows are shown.'';

       end;

*/

    end if;

      htp.prn(''</PLSQL>'');

/*	  </td></tr><tr>

<td></br><FONT ID="B30SPOROCILO_1" CLASS="FONT">'' ||

              B30SPOROCILO(1) || ''</FONT></td>

</tr></table>

</div>'');*/



end if;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>GBUTTONPREV</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p( ''<input type=button class="';
 v_vc(16) := 'rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>GBUTTONPREV</rblockid></trigger><trigger><blockid>HINTCONTENT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>HINTCONTENT</rblockid></trigger><trigger><blockid></blockid><triggerid>POST_ACTION</triggerid><plsql><![CDATA[

if action = GBUTTONSUBMIT then



pselect;

poutput;



end if;
]]></plsql><plsqlspec><![CDATA[  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR ) then
    raise_application_error(''-20000'', ''ACTION="''||ACTION||''" is not defined. Define it in POST_ACTION trigger.'');
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_SUBMIT</triggerid><plsql><![CDATA[----put procedure in the begining of trigger;

post_submit_template;



      if b10textarea(1) is not null then



          if instr(upper(b10textarea(1)), ''DESCR'') = 1 then

            b10textarea(1) := ''select column_name, data_type,data_length, d';
 v_vc(17) := 'ecode(nullable,''''N'''',''''NOT NULL'''','''''''')

                               from all_tab_cols t

                               where table_name = '''''' ||

                               upper(ltrim(rtrim(substr(b10textarea(1), 6)))) || '''''''';

          end if;





--check exists of EXECUTESQL

    declare

      n number;

	  nxxx number;

	  verr varchar2(32000);

    begin

        select count(*) into nxxx from all_objects o where o.object_name = ''EXECUTESQL'' and o.object_type = ''PACKAGE'' and o.owner = user;







        if nxxx = 0 then

            b30sporocilo(1) := ''<FONT color=red>Package EXECUTESQL missing in your schema (''||user||''). Compile form EXECUTESQL on your schema (select form from suported RASD application form list) and run it to execute grants.</FONT>'';

			ERROR := b30sporocilo;

        else

          execute immediate

''

declare ns number; ni number; nd number; nu number; ve varchar2(1000); vret varchar2(1000);

begin

   :1 := ''||user||''.EXECUTESQL.run(:2, :3);

end;

''

using out verr, in b10textarea(1), out n

;

          if verr is not null then

                        b30sporocilo(1) := ''<FONT color=red>Logged user: ''||user||'':'' || verr || ''</FONT>'';

						ERROR := B30SPOROCILO;

          else

             if instr(upper(b10textarea(1)), ''SELECT'') > 0 then

                 b30sporocilo(1) := ''Only first 100 rows are shown.'';

             else

               if instr(upper(b10textarea(1)), ''UPDATE'') > 0 then

                  b30sporocilo(1) := '''' || n || '' rows were updated.'';

               elsif instr(upper(b10textarea(1)), ''INSERT'') > 0 then

                  b30sporocilo(1) := '''' || n || '' rows were inserted.'';

                elsif instr(upper(b10textarea(1)), ''DELETE'') > 0 then

                  b30sporocilo(1) := '''' || n || '' rows were deleted.'';

                else

                  b30sporocilo(1) := '''' || n || '' rows were efected.'';

                end if;

             end if;

          end if;





        end if;



    end;

-----------

        -- IN SQL, LANG

        -- OUT MESSAGE, NUMBER ,

/*

        declare

          cid     pls_integer;

          n       pls_integer;

          ni';
 v_vc(18) := 'nsert number;

          ndelete number;

          nupdate number;

        begin

          cid := dbms_sql.open_cursor;

          dbms_sql.parse(cid, b10textarea(1), dbms_sql.native);

          n := dbms_sql.execute(cid);

          if n >= 0 then

            nselect := instr(upper(b10textarea(1)), ''SELECT'');

            ninsert := instr(upper(b10textarea(1)), ''INSERT'');

            nupdate := instr(upper(b10textarea(1)), ''UPDATE'');

            ndelete := instr(upper(b10textarea(1)), ''DELETE'');

            if nselect = 1 then

              -- PLSQL CODE run on UI

              b30sporocilo(1) := ''Only first 100 rows are shown.'';

            elsif ninsert = 1 then

              b30sporocilo(1) := '''' || n || '' rows were inserted.'';

            elsif ndelete = 1 then

              b30sporocilo(1) := '''' || n || '' rows were deleted.'';

            elsif nupdate = 1 then

              b30sporocilo(1) := '''' || n || '' rows were updated.'';

            end if;

          end if;

        exception

          when others then

            b30sporocilo(1) := ''<FONT color=red>Logged user: ''||user||'':'' || sqlerrm || ''</FONT>'';

        end;

*/





      end if;
]]></plsql><plsqlspec><![CDATA[-- Executing after filling fields on submit.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10</blockid><triggerid>POST_UI</triggerid><plsql><![CDATA[htp.p(''<div class="label">'');

htp.p(RASDI_TRNSLT.text(''Press CTRL+SPACE to open variable list, CTRL+ALT to get DML code, CTRL+F to search, CTRL-I fold open, CTRL-Y fold close,  ALT+G jump to line, F11 to maximize window. (fold string ----)'',lang));

htp.p(''<a onclick="$(''''#SESSSTORAGEENABLED_1_RASD'''').val(''''''''); document.cookie=''''rasdi$SESSSTORAGEENABLED='''';">''||RASDI_TRNSLT.text(''Click here to refresh DB cache in editor'',lang)||''</a>'');

htp.p(''</div>'');


]]></plsql><plsqlspec><![CDATA[-- Triggers before afther BLOCK content.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>_ changes</triggerid><plsql><![CDATA[  function changes(p_log out varchar2) return varchar2 is

  begin



    p_log := ''/* Change LOG:

20230301 - Created new 2 version

20171219 - Changed SQL execution. Now uses local package EXECUTESQL

';
 v_vc(19) := '20171212 - Added SQL logging, ...

20160310 - Included CodeMirror

20141027 - Added footer on all pages

*/'';

    return version;

 end;
]]></plsql><plsqlspec><![CDATA[  function changes(p_log out varchar2) return varchar2 ;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>codemirror</triggerid><plsql><![CDATA[procedure htpClob(v_clob clob);



function codemirrorrasd(pf varchar2) return clob is

 v_ret clob;

 v_currentDADUser varchar2(50) := rasdc_library.currentDADUser;



begin



v_ret := v_ret  ||'' __RASD_VARIABLES: {},'';

for r in (  SELECT blockid, fieldid, blockid||fieldid polje

            FROM RASD_FIELDS

           where formid = pf

           order by nvl(blockid,''.''), fieldid) loop

v_ret := v_ret  ||''      "''||r.polje||''": {},'';

end loop;

for r in (  SELECT blockid, fieldid, blockid||fieldid||''#SET'' polje

            FROM RASD_FIELDS f

           where formid = pf

             and includevis = ''Y''

           order by nvl(blockid,''.''), fieldid) loop

v_ret := v_ret  ||''      "''||r.polje||''.visible":{},'';

v_ret := v_ret  ||''      "''||r.polje||''.disabled":{},'';

v_ret := v_ret  ||''      "''||r.polje||''.readonly":{},'';

v_ret := v_ret  ||''      "''||r.polje||''.required":{},'';

v_ret := v_ret  ||''      "''||r.polje||''.error":{},'';

v_ret := v_ret  ||''      "''||r.polje||''.info":{},'';

v_ret := v_ret  ||''      "''||r.polje||''.custom":{},'';

end loop;

v_ret := v_ret  ||''"__RASD_PROCEDURES:" : {},'';

v_ret := v_ret  ||''"rlog( message VARCHAR2 );" : {},'';

v_ret := v_ret  ||''"htpClob( text CLOB );" : {},'';

v_ret := v_ret  ||''"openLOV( p_lov varchar2,  p_value varchar2) return lovtab__ (label VARHAR2,id VARCHAR2)" : {},'';

v_ret := v_ret  ||''"version" : {},'';

v_ret := v_ret  ||''"this_form" : {},'';

v_ret := v_ret  ||''"psubmit(name_array, value_array)" : {},'';

v_ret := v_ret  ||''"pclear" : {},'';

v_ret := v_ret  ||''"pselect" : {},'';

v_ret := v_ret  ||''"pcommit" : {},'';

v_ret := v_ret  ||''"poutput" : {},'';



for rx in (

  select 1 from rasd_forms f where f.formid = pf

  and f.autocreaterestyn = ''Y''

) loop

v_ret := v_ret  ||''"poutputrest" : {},'';

v_ret := v_ret  ||''"poutputrest() return clob" : {},'';

end loop;



for r in (select blockid from rasd_blocks b where b.formid = ';
 v_vc(20) := 'pf order by blockid)

loop

v_ret := v_ret  ||''"pclear_''||r.blockid||''(pstart number)" : {},'';

v_ret := v_ret  ||''"pselect_''||r.blockid||''" : {},'';

v_ret := v_ret  ||''"pcommit_''||r.blockid||''" : {},'';

end loop;



return v_ret;



end;



function codemirrordb(pf varchar2) return clob is

 v_ret clob;

 v_currentDADUser varchar2(50) := rasdc_library.currentDADUser;



function add_attributes(powner varchar2, pname varchar2, pprocedure varchar2) return varchar2 is

   v_ret varchar2(32000);

 begin

for r in (

select x.argument_name, x.data_type from all_arguments x where

  x.owner = powner

  and x.object_name = pprocedure

  and x.package_name = pname

  and x.argument_name is not null

order by sequence

) loop

v_ret := v_ret||'',''||r.argument_name||'' ''||r.data_type;

end loop;

if v_ret is null then

   return '''';

else

   return ''(''||substr(v_ret,''2'')||'')'';

end if;

end;



function add_columns (powner varchar2, pname varchar2) return varchar2 is

 v_fir number := 1;

 v_ret varchar2(32000) := '''';

 begin



for r in (

select a.owner, a.table_name o_name,a.column_name c_name, a.column_id vr, ''T'' vir from all_tab_columns a

where a.owner = powner

  and column_name is not null

  and a.table_name = pname

  and a.column_name not like ''%$%''

  and a.column_name not like ''%#%''

union

select p.owner, p.object_name, p.procedure_name , p.subprogram_id  , ''P'' from all_procedures p

where p.owner = powner

  and procedure_name is not null

  and p.object_name = pname

  and p.procedure_name not like ''%$%''

  and p.procedure_name not like ''%#%''

order by vr

) loop

if r.vir = ''P'' then

  if v_fir = 1 then

v_ret := v_ret ||''       "''||r.c_name||add_attributes(powner, pname,r.c_name)||''" '';

     v_fir := 2;

  else

v_ret := v_ret ||''      , "''||r.c_name||add_attributes(powner, pname,r.c_name)||''" '';

  end if;

else

  if v_fir = 1 then

v_ret := v_ret ||''       "''||r.c_name||''" '';

     v_fir := 2;

  else

v_ret := v_ret ||''      , "''||r.c_name||''" '';

  end if;

end if;

end loop;



return v_ret;

 end;



begin



for r in (

                  select /*+ RULE*/ owner ,OBJECT_NAME id,

                          OBJECT_NAME || '' ... '' || substr(object_type, 1, 1) label,

                          2 x

                    from all_objects

                   where object_type in (''TABL';
 v_vc(21) := 'E'', ''VIEW'')

                     and (owner = v_currentDADUser)

                     and object_name not like ''%$%''

                     and object_name not like ''%#%''

                  union

                  select /*+ RULE*/ distinct table_owner , SYNONYM_NAME id,

                                   SYNONYM_NAME || '' ... S'' label,

                                   2 x

                    from user_synonyms s, all_tab_columns tc

                   where s.table_name = tc.table_name

                     and s.table_owner = tc.owner

                     and s.table_name not like ''%$%''

                     and s.table_name not like ''%#%''

                   union

                   select distinct owner , table_name id,

                          owner||''.''||table_name  /*|| '' ... '' || substr(type, 1, 1) */ label, 2 x

                   from dba_tab_privs x

                   where

                    grantee = v_currentDADUser

                    and table_name not like ''%$%''

                    and table_name not like ''%#%''

                   order by  1, 2

) loop

if r.owner = v_currentDADUser then

v_ret := v_ret  ||''      , "''||r.id||''": ['';

else

v_ret := v_ret  ||''      , "''||r.owner||''.''||r.id||''": ['';

end if;

v_ret := v_ret  || add_columns (r.owner, r.id) ;

v_ret := v_ret  ||''] '';

end loop;



return v_ret;



end;





procedure codemirrorjs(n varchar2, h varchar2, d varchar2, f varchar2) is

  d1 varchar2(100) := d;

  d2 varchar2(100);

begin



if d is null then

d2 := rasdi_client.sessionGetValue(''SESSSTORAGEENABLED'');

if d2 is null then

rasdi_client.sessionSetValue(''SESSSTORAGEENABLED'', to_char(sysdate,''ddmmyyyyhh24miss'') );

rasdi_client.sessionclose;

else

d1:=d2;

end if;

end if;



htp.p(''

function rasd_codemirror_''||n||''() {



var mime = "text/x-sql";

  // get mime type

  if (window.location.href.indexOf("mime=") > -1) {

    mime = window.location.href.substr(window.location.href.indexOf("mime=") + 5);

  }

'');





htp.p(''var rasdhintoptions;'');



if d1 is null then

htp.p(''rasdhintoptions = sessionStorage.getItem("rasdi$UserDbHint"); if (rasdhintoptions == null) {rasdhintoptions = '''''''';} '');

htp.p(''if (rasdhintoptions.length == 0) {'');

htp.prn(''rasdhintoptions = '''''');

htpClob(codemirrordb(f));

htp.p('''''';'');

htp.p(''sessionStorage.setItem';
 v_vc(22) := '("rasdi$UserDbHint",rasdhintoptions);'');

--htp.p(''} else {'');

--htp.p(''rasdhintoptions = rasdhintoptions; '');

htp.p(''}'');

htp.p(''$("#SESSSTORAGEENABLED_1_RASD").val("''||to_char(sysdate,''ddmmyyyyhh24miss'')||''");'');

else

htp.p(''rasdhintoptions = sessionStorage.getItem("rasdi$UserDbHint"); if (rasdhintoptions == null) {rasdhintoptions = '''''''';} '');

end if;





htp.prn(''var rasdhintoptionsuser = '''''');

htp.prn(''{tables: {'');

htpClob(codemirrorrasd(f));

htp.prn('' __USER_OBJECTS: {}'');

htp.prn(''''''+rasdhintoptions+'''''');

htp.prn('', __OTHER_FUNCTIONS: {}'');

htp.p(''    }}'''';'');





htp.p('' var rasdhintoptionsobj = eval(''''(''''+rasdhintoptionsuser+'''')'''');'');



htp.p(''

if (document.getElementById("''||n||''") != null) {



window.rasd_CMEditor''||n||'' = CodeMirror.fromTextArea(document.getElementById("''||n||''"), {

    mode: "sql",

    indentWithTabs: true,

    smartIndent: true,

    lineNumbers: true,

	lineWrapping: true,

    matchBrackets : true,

    autofocus: true,

    foldGutter: true,

	gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],

    foldOptions: {

  rangeFinder: (cm, pos) => {

    var line=window.rasd_CMEditor''||n||''.getLine(pos.line);

	  var match=line.match(/---- /);



    if (match==null) {

      return CodeMirror.fold.auto(cm,pos);

    }

    else {

      var lineend = window.rasd_CMEditor''||n||''.lineCount();

      for (let i = pos.line+1; i < lineend; i++) {

         if ( window.rasd_CMEditor''||n||''.getLine(i).match(/---- /) == null )

            {}

         else

            {lineend = i-1; break; }

      }

        var startPos=CodeMirror.Pos(pos.line+1, 0);

	    var endPos=CodeMirror.Pos(lineend, 0);

      return endPos && CodeMirror.cmpPos(endPos, startPos) > 0 ? {from: startPos, to: endPos} : null;

    }

  }

    },

    extraKeys: {"Ctrl-Space": "autocomplete",

	            "Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); },

	            "Ctrl-Y": cm => CodeMirror.commands.foldAll(cm),

                "Ctrl-I": cm => CodeMirror.commands.unfoldAll(cm),

                "Alt-S": function(instance) { document.''||this_form||''.ACTION.value=''''''||GBUTTONSAVE||''''''; document.''||this_form||''.submit(); },

                "Ctrl-Alt": function(instance) { if ($("#B1_DIV").css("display") ==="block") {$("#B1_DIV").css("display","none")} else { $("#B1_DIV").css("display",';
 v_vc(23) := '"block")} },

	            "F11": function(cm) {cm.setOption("fullScreen", !cm.getOption("fullScreen"));},

	            "Esc": function(cm) { if (cm.getOption("fullScreen")) {cm.setOption("fullScreen", false);}  if ($("#B1_DIV").css("display") ==="block") {$("#B1_DIV").css("display","none")} }

	  },

    hintOptions: rasdhintoptionsobj

    });



  //window.rasd_CMEditor''||n||''.foldCode(CodeMirror.Pos(1, 0));

}



}'');





end;






]]></plsql><plsqlspec><![CDATA[procedure codemirrorjs(n varchar2, h varchar2, d varchar2, f varchar2);
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure custom template</triggerid><plsql><![CDATA[function showLabel(plabel varchar2, pcolor varchar2 default ''U'', pshowdialog number default 0)

return varchar2 is

   v__ varchar2(32000);

begin



v__ := RASDI_TRNSLT.text(plabel, lang);



if pshowdialog = 1 then

v__ := v__ || rasdc_hints.linkDialog(replace(replace(plabel,'' '',''''),''.'',''''),lang,replace(this_form,''2'','''')||''_DIALOG'');

end if;



if pcolor is null then



return v__;



else



return ''<font color="''||pcolor||''">''||v__||''</font>'';



end if;





end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure post_submit template</triggerid><plsql><![CDATA[procedure post_submit_template is

begin

RASDC_LIBRARY.checkprivileges(PFORMID);

begin

      select upper(form)

        into pform

        from RASD_FORMS

       where formid = PFORMID;

exception when others then null;

end;



if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then

  if rasdc_library.allowEditing(pformid) then

     null;

  else

     ACTION := GBUTTONSRC;

	 message := message || RASDI_TRNSLT.text(''User has no privileges to save data!'', lang);

  end if;

end if;





if rasdc_library.allowEditing(pformid) then

   GBUTTONSAVE#SET.visible := true;

   GBUTTONCOMPILE#SET.visible := true;

else

   GBUTTONSAVE#SET.visible := false;

   GBUTTONCOMPILE#SET.visible := false;

end if;





VUSER := rasdi_client.secGetUsername;

VLOB := rasdi_client.secGetLOB;

if lang is null then lang := rasdi_client.c_defaultLanguage; end if;



end;
]]></plsql><plsq';
 v_vc(24) := 'lspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure_compile_template</triggerid><plsql><![CDATA[procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''''  , pcompid in out number) is

      v_server RASD_ENGINES.server%type;

      cid      pls_integer;

      n        pls_integer;

      vup      varchar2(30) := rasdi_client.secGetUsername;

begin

        rasdc_library.log(this_form,pformid, ''COMPILE_S'', pcompid);



        begin

          if instr( upper(rasd_client.secGet_PATH_INFO), upper(pform)) > 0

			 then

            sporocilo := RASDI_TRNSLT.text(''From is not generated.'', lang);

          else



            select server

              into v_server

              from RASD_FORMS_COMPILED fg, RASD_ENGINES g

             where fg.engineid = g.engineid

               and fg.formid = PFORMID

               and fg.editor = vup

               and (fg.lobid = rasdi_client.secGetLOB or

                   fg.lobid is null and rasdi_client.secGetLOB is null);



            cid     := dbms_sql.open_cursor;



            dbms_sql.parse(cid,

                           ''begin '' || v_server || ''.c_debug := false;''|| v_server || ''.form('' || PFORMID ||

                           '','''''' || lang || '''''');end;'',

                           dbms_sql.native);



            n       := dbms_sql.execute(cid);



            dbms_sql.close_cursor(cid);



            sporocilo := RASDI_TRNSLT.text(''From is generated.'', lang) || rasdc_library.checknumberofsubfields(PFORMID);



        if refform is not null then

           sporocilo :=  sporocilo || ''<br/> - ''||  RASDI_TRNSLT.text(''To unlink referenced code check:'', lang)||''<input type="checkbox" name="UNLINK" value="Y"/>.'';

        end if;



          end if;

        exception

          when others then

            if sqlcode = -24344 then



            sporocilo := RASDI_TRNSLT.text(''Form is generated with compilation error. Check your code.'', lang)||''(''||sqlerrm||'')'';



            else

            sporocilo := RASDI_TRNSLT.text(''Form is NOT ';
 v_vc(25) := 'generated - internal RASD error.'', lang) || ''(''||sqlerrm||'')<br>''||

                         RASDI_TRNSLT.text(''To debug run: '', lang) || ''begin '' || v_server || ''.form('' || PFORMID ||

                         '','''''' || lang || '''''');end;'' ;

            end if;

        end;

        rasdc_library.log(this_form,pformid, ''COMPILE_E'', pcompid);

end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger></triggers><elements><element><elementid>1</elementid><pelementid>0</pelementid><orderby>1</orderby><element>HTML_</element><type></type><id>HTML</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>2</elementid><pelementid>1</pelementid><orderby>1</orderby><element>HEAD_</element><type></type><id>HEAD</id><nameid>HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<head]]></name';
 v_vc(26) := '><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''<%= rasdc_library.formName(PFORMID, LANG) %>''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');
%>]]></value><valuecode><![CDATA['');
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''''|| rasdc_library.formName(PFORMID, LANG) ||''''));
htp.p('''');
htp.p(''<script type="text/javascript">'');
formgen_js;
htp.p(''</script>'');
htpClob(FORM_UIHEAD);
htp.p(''<style type="text/css">'');
htpClob(FORM_CSS);
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>'');

htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>15</elementid><pelementid>1</pelementid><orderby>1</orderby><element>BODY_</element><type></type><id>BODY</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><r';
 v_vc(27) := 'id></rid></attribute></attributes></element><element><elementid>16</elementid><pelementid>15</pelementid><orderby>3</orderby><element>FORM_</element><type>F</type><id>RASDC2_SQLCLIENT</id><nameid>RASDC2_SQLCLIENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_ACTION</attribute><type>A</type><text></text><name><![CDATA[action]]></name><value><![CDATA[?]]></value><valuecode><![CDATA[="?"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_METHOD</attribute><type>A</type><text></text><name><![CDATA[method]]></name><value><![CDATA[post]]></value><valuecode><![CDATA[="post"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RASDC2_SQLCLIENT]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>17</elementid><pelementid>15</pelementid><orderby>1</orderby><e';
 v_vc(28) := 'lement>FORM_LAB</element><type>F</type><id>RASDC2_SQLCLIENT_LAB</id><nameid>RASDC2_SQLCLIENT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormLab]]></value><valuecode><![CDATA[="rasdFormLab"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_LAB]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlHeaderDataTable(''RASDC2_SQLCLIENT_LAB'',''<%= rasdc_library.formName(PFORMID, LANG) %>'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlHeaderDataTable(''RASDC2_SQLCLIENT_LAB'',''''|| rasdc_library.formName(PFORMID, LANG) ||'''') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><ele';
 v_vc(29) := 'ment><elementid>18</elementid><pelementid>15</pelementid><orderby>2</orderby><element>FORM_MENU</element><type>F</type><id>RASDC2_SQLCLIENT_MENU</id><nameid>RASDC2_SQLCLIENT_MENU</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMenu]]></value><valuecode><![CDATA[="rasdFormMenu"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_MENU]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_MENU"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlMenuList(''RASDC2_SQLCLIENT_MENU'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlMenuList(''RASDC2_SQLCLIENT_MENU'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>19</e';
 v_vc(30) := 'lementid><pelementid>15</pelementid><orderby>9999</orderby><element>FORM_BOTTOM</element><type>F</type><id>RASDC2_SQLCLIENT_BOTTOM</id><nameid>RASDC2_SQLCLIENT_BOTTOM</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBottom]]></value><valuecode><![CDATA[="rasdFormBottom"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_BOTTOM]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_BOTTOM"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlFooter(version , substr(''RASDC2_SQLCLIENT_BOTTOM'',1,instr(''RASDC2_SQLCLIENT_BOTTOM'', ''_'',-1)-1) , '''') %>]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlFooter(version , substr(''RASDC2_SQLCLIENT_BOTTOM'',1,instr(''RASDC2_SQLCLIENT_BOTTOM'', ''_'',-1)-1) , '''') ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><t';
 v_vc(31) := 'extcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>20</elementid><pelementid>16</pelementid><orderby>3</orderby><element>FORM_DIV</element><type>F</type><id>RASDC2_SQLCLIENT_DIV</id><nameid>RASDC2_SQLCLIENT_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdForm]]></value><valuecode><![CDATA[="rasdForm"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_DIV]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>21</elementid><pelementid>20</pelementid><orderby>1</orderby><element>FORM_HEAD</element><type>F</type><id>RASDC2_SQLCLIENT_HEAD</id><nameid>RASDC2_SQLCLIENT_HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A';
 v_vc(32) := '</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormHead]]></value><valuecode><![CDATA[="rasdFormHead"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_HEAD]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_HEAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>22</elementid><pelementid>20</pelementid><orderby>5</orderby><element>FORM_BODY</element><type>F</type><id>RASDC2_SQLCLIENT_BODY</id><nameid>RASDC2_SQLCLIENT_BODY</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBody]]></value><valuecode><![CDATA[="rasdFormBody"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_BODY]]></value><valuecod';
 v_vc(33) := 'e><![CDATA[="RASDC2_SQLCLIENT_BODY"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>23</elementid><pelementid>20</pelementid><orderby>9999</orderby><element>FORM_FOOTER</element><type>F</type><id>RASDC2_SQLCLIENT_FOOTER</id><nameid>RASDC2_SQLCLIENT_FOOTER</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormFooter]]></value><valuecode><![CDATA[="rasdFormFooter"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_FOOTER]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_FOOTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn';
 v_vc(34) := '>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>24</elementid><pelementid>20</pelementid><orderby>2</orderby><element>FORM_RESPONSE</element><type>F</type><id>RASDC2_SQLCLIENT_RESPONSE</id><nameid>RASDC2_SQLCLIENT_RESPONSE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormResponse]]></value><valuecode><![CDATA[="rasdFormResponse"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_RESPONSE]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_RESPONSE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><r';
 v_vc(35) := 'id></rid></attribute></attributes></element><element><elementid>25</elementid><pelementid>24</pelementid><orderby>9998</orderby><element>FORM_MESSAGE</element><type>F</type><id>RASDC2_SQLCLIENT_MESSAGE</id><nameid>RASDC2_SQLCLIENT_MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage]]></value><valuecode><![CDATA[="rasdFormMessage"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_MESSAGE]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_MESSAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>26</elementid><pelementid>24</pelementid><orderby>9996</orderby><element>FORM_ERROR</element><type>F</type><id>RASDC2_SQLCLIENT_ERROR</id><nameid>RASDC2_SQLCLIENT_ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><te';
 v_vc(36) := 'xt></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage error]]></value><valuecode><![CDATA[="rasdFormMessage error"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_SQLCLIENT_ERROR]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_ERROR"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>27</elementid><pelementid>24</pelementid><orderby>9997</orderby><element>FORM_WARNING</element><type>F</type><id>RASDC2_SQLCLIENT_WARNING</id><nameid>RASDC2_SQLCLIENT_WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage warning]]></value><valuecode><![CDATA[="rasdFormMessage warning"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDA';
 v_vc(37) := 'TA[RASDC2_SQLCLIENT_WARNING]]></value><valuecode><![CDATA[="RASDC2_SQLCLIENT_WARNING"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>28</elementid><pelementid>22</pelementid><orderby>105</orderby><element>BLOCK_DIV</element><type>B</type><id>P_DIV</id><nameid>P_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_DIV]]></value><valuecode><![CDATA[="P_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid>';
 v_vc(38) := '</textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockP_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockP_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>29</elementid><pelementid>28</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>30</elementid><pelement';
 v_vc(39) := 'id>29</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>P_LAB</id><nameid>P_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_LAB]]></value><valuecode><![CDATA[="P_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>31</elementid><pelementid>28</pelementid><orderby>106</orderby><element>TABLE_</element><type></type><id>P_TABLE</id><nameid>P_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis';
 v_vc(40) := '></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_TABLE_RASD]]></value><valuecode><![CDATA[="P_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>32</elementid><pelementid>31</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>P_BLOCK</id><nameid>P_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_BLOCK_NAME]]></value><valuecode><![CDATA[="P_BLOCK_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]';
 v_vc(41) := '></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>33</elementid><pelementid>22</pelementid><orderby>115</orderby><element>BLOCK_DIV</element><type>B</type><id>B10_DIV</id><nameid>B10_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_DIV]]></value><valuecode><![CDATA[="B10_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></text';
 v_vc(42) := 'id><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB10_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB10_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>34</elementid><pelementid>33</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>35</elementid><pelementid>34</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B10_LAB</id><nameid>B10_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelb';
 v_vc(43) := 'lock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_LAB]]></value><valuecode><![CDATA[="B10_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>36</elementid><pelementid>33</pelementid><orderby>116</orderby><element>TABLE_</element><type></type><id>B10_TABLE</id><nameid>B10_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attrib';
 v_vc(44) := 'ute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_TABLE_RASD]]></value><valuecode><![CDATA[="B10_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>37</elementid><pelementid>36</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B10_BLOCK</id><nameid>B10_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_BLOCK_NAME]]></value><valuecode><![CDATA[="B10_BLOCK_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><!';
 v_vc(45) := '[CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>38</elementid><pelementid>22</pelementid><orderby>125</orderby><element>BLOCK_DIV</element><type>B</type><id>B1_DIV</id><nameid>B1_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_DIV]]></value><valuecode><![CDATA[="B1_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB1_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB1_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hi';
 v_vc(46) := 'ddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>39</elementid><pelementid>38</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>40</elementid><pelementid>39</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B1_LAB</id><nameid>B1_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_LAB]]></val';
 v_vc(47) := 'ue><valuecode><![CDATA[="B1_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>41</elementid><pelementid>38</pelementid><orderby>126</orderby><element>TABLE_N</element><type></type><id>B1_TABLE</id><nameid>B1_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></r';
 v_vc(48) := 'id></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_TABLE_RASD]]></value><valuecode><![CDATA[="B1_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>42</elementid><pelementid>41</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B1_BLOCK</id><nameid>B1_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1_BLOCK_NAME]]></value><valuecode><![CDATA[="B1_BLOCK_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop;
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]';
 v_vc(49) := ']></value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB1 in 1..B1OWNER_ID.count loop
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>43</elementid><pelementid>41</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B1_THEAD</id><nameid>B1_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>44</elementid><pelementid>43</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid>';
 v_vc(50) := '</rid></attribute></attributes></element><element><elementid>45</elementid><pelementid>44</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B1OWNER_ID_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB1]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB1OWNER_ID]]></value><valuecode><![CDATA[="rasdTxLabB1OWNER_ID"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>46</elementid><pelementid>45</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B1OWNER_ID_LAB</id><nameid>B1OWNER_ID_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labe';
 v_vc(51) := 'l]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1OWNER_ID_LAB]]></value><valuecode><![CDATA[="B1OWNER_ID_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>47</elementid><pelementid>44</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>B1DML_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB1]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlob';
 v_vc(52) := 'id></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB1DML]]></value><valuecode><![CDATA[="rasdTxLabB1DML"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>48</elementid><pelementid>47</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>B1DML_LAB</id><nameid>B1DML_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1DML_LAB]]></value><valuecode><![CDATA[="B1DML_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value>';
 v_vc(53) := '<![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>49</elementid><pelementid>22</pelementid><orderby>135</orderby><element>BLOCK_DIV</element><type>B</type><id>B30_DIV</id><nameid>B30_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_DIV]]></value><valuecode><![CDATA[="B30_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></at';
 v_vc(54) := 'tribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB30_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB30_DIV  then
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>50</elementid><pelementid>49</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>51</elementid><pelementid>50</pelementid><orderby>1</orderby><element>FONT_</element><type>B</ty';
 v_vc(55) := 'pe><id>B30_LAB</id><nameid>B30_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_LAB]]></value><valuecode><![CDATA[="B30_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>52</elementid><pelementid>49</pelementid><orderby>136</orderby><element>TABLE_</element><type></type><id>B30_TABLE</id><nameid>B30_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><att';
 v_vc(56) := 'ribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_TABLE_RASD]]></value><valuecode><![CDATA[="B30_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>53</elementid><pelementid>52</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B30_BLOCK</id><nameid>B30_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_BLOCK_NAME]]></value><valuecode><![CDATA[="B30_BLOCK_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><for';
 v_vc(57) := 'loop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>54</elementid><pelementid>21</pelementid><orderby>-9</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB10</id><nameid>RECNUMB10</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMB10_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB10_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB10_NAME]]></value><valuecode><![CDATA[="RECNUMB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></r';
 v_vc(58) := 'id></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB10_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMB10))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>55</elementid><pelementid>21</pelementid><orderby>-1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMP</id><nameid>RECNUMP</namei';
 v_vc(59) := 'd><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMP_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMP_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMP_NAME]]></value><valuecode><![CDATA[="RECNUMP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby';
 v_vc(60) := '><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMP_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMP))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>56</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>ACTION</id><nameid>ACTION</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddeny';
 v_vc(61) := 'n>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ACTION_NAME_RASD]]></value><valuecode><![CDATA[="ACTION_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[ACTION_NAME]]></value><valuecode><![CDATA[="ACTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[ACTION_VALUE]]></value><valuecode><![CDATA[="''||ACTION||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><';
 v_vc(62) := 'orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>57</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PAGE</id><nameid>PAGE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PAGE_NAME_RASD]]></value><valuecode><![CDATA[="PAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PAGE_NAME]]></value><valuecode><![CDATA[="PAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hidde';
 v_vc(63) := 'nyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PAGE_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(PAGE))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>58</elementid><pelementid>21</pelementid><orderby>6</o';
 v_vc(64) := 'rderby><element>INPUT_HIDDEN</element><type>D</type><id>LANG</id><nameid>LANG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[LANG_NAME_RASD]]></value><valuecode><![CDATA[="LANG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[LANG_NAME]]></value><valuecode><![CDATA[="LANG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlo';
 v_vc(65) := 'bid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[LANG_VALUE]]></value><valuecode><![CDATA[="''||LANG||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>59</elementid><pelementid>21</pelementid><orderby>7</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PFORMID</id><nameid>PFORMID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></';
 v_vc(66) := 'forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFORMID_NAME_RASD]]></value><valuecode><![CDATA[="PFORMID_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PFORMID_NAME]]></value><valuecode><![CDATA[="PFORMID"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PFORMID_VALUE]]></value><valuecode><![CDATA[="''||PFORMID||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid><';
 v_vc(67) := '/rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>60</elementid><pelementid>21</pelementid><orderby>13</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlo';
 v_vc(68) := 'bid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><';
 v_vc(69) := 'attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></n';
 v_vc(70) := 'ame><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>61</elementid><pelementid>23</pel';
 v_vc(71) := 'ementid><orderby>10011</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</o';
 v_vc(72) := 'rderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</';
 v_vc(73) := 'attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></v';
 v_vc(74) := 'alueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSAVE  then
htp.prn('''');  if GBUTTONSAVE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>62</elementid><pelementid>21</pelementid><orderby>14</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTO';
 v_vc(75) := 'NCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textc';
 v_vc(76) := 'ode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p';
 v_vc(77) := '('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><at';
 v_vc(78) := 'tribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>63</elementid><pelementid>23</pelementid><orderby>10012</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><fo';
 v_vc(79) := 'rloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>';
 v_vc(80) := 'N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid';
 v_vc(81) := '></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONCOMPILE  then
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source>';
 v_vc(82) := '</source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>64</elementid><pelementid>21</pelementid><orderby>15</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop';
 v_vc(83) := '><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp';
 v_vc(84) := '.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></fo';
 v_vc(85) := 'rloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>65</elementid><pelementid>23</pelementid><orderby>10013</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GB';
 v_vc(86) := 'UTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribut';
 v_vc(87) := 'e>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="r';
 v_vc(88) := 'equired"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valuei';
 v_vc(89) := 'd></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONRES  then
htp.prn('''');  if GBUTTONRES#SET.visible then
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>66</elementid><pelementid>21</pelementid><orderby>16</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p';
 v_vc(90) := '('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid';
 v_vc(91) := '><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>67</elementid><pelementid>23</pelementid><orderby>10014</orderby><element>PLSQL_</element><type>D</typ';
 v_vc(92) := 'e><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></';
 v_vc(93) := 'valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONPREV  then
htp.prn('''');  if GBUTTONPREV#SET.visible then
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;
htp.prn('']]></valueco';
 v_vc(94) := 'de><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||

                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||

                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,

                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',

                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,

                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||

                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

									 ,

                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''

                                )

			       ) || ''>''

);
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>68</elementid><pelementid>26</pelementid><orderby>10046</orderby><element>FONT_</element><type>D</type><id>ERROR</id><nameid>ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><';
 v_vc(95) := 'source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ERROR_NAME_RASD]]></value><valuecode><![CDATA[="ERROR_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><!';
 v_vc(96) := '[CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[ERROR_VALUE]]></value><valuecode><![CDATA[''||ERROR||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>69</elementid><pelementid>25</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>MESSAGE</id><nameid>MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[MESSAGE_NAME_RASD]]></value><valuecode><![CDATA[="MESSAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid><';
 v_vc(97) := '/rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hidden';
 v_vc(98) := 'yn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[MESSAGE_VALUE]]></value><valuecode><![CDATA[''||MESSAGE||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>70</elementid><pelementid>27</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>WARNING</id><nameid>WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[WARNING_NAME_RASD]]></value><valuecode><![CDATA[="WARNING_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDA';
 v_vc(99) := 'TA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[WARNING_VALUE]]></value><valuecode><![CDATA[''||WARNING||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></';
 v_vc(100) := 'attributes></element><element><elementid>71</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>72</elementid><pelementid>23</pelementid><orderby>300874</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></';
 v_vc(101) := 'includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldHINTCONTENT  then
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<div style="display: none;">'');

htp.p(rasdc_hints.getHint(replace(this_form,''2'','''')||''_DIALOG'',lang));

htp.p(''</div>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>73</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSUBMIT</id><nameid>GBUTTONSUBMIT</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></va';
 v_vc(102) := 'lueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSUBMIT_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSUBMIT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSUBMIT_NAME]]></value><valuecode><![CDATA[="GBUTTONSUBMIT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rfor';
 v_vc(103) := 'm></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSUBMIT_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSUBMIT||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSUBMIT  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>74</elementid><pelementid>23</pelementid><orderby>300874</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSUBMIT</id><nameid>GBUTTONSUBMIT</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CD';
 v_vc(104) := 'ATA[id]]></name><value><![CDATA[GBUTTONSUBMIT_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSUBMIT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSUBMIT_NAME]]></value><valuecode><![CDATA[="GBUTTONSUBMIT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSUBMIT_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSUBMIT||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></nam';
 v_vc(105) := 'e><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');
if  ShowFieldGBUTTONSUBMIT  then
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>75</elementid><pelementid>32</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>P</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[P_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hi';
 v_vc(106) := 'ddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>76</elementid><pelementid>75</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>SESSSTORAGEENABLED</id><nameid>SESSSTORAGEENABLED</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[SESSSTORAGEENABLED_NAME_RASD]]></value><valuecode><![CDATA[="SESSSTORAGEENABLED_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attrib';
 v_vc(107) := 'ute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[SESSSTORAGEENABLED_NAME]]></value><valuecode><![CDATA[="SESSSTORAGEENABLED_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[SESSSTORAGEENABLED_VALUE]]></value><valuecode><![CDATA[="''||PSESSSTORAGEENABLED(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></';
 v_vc(108) := 'forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>77</elementid><pelementid>37</pelementid><orderby>19</orderby><element>TX_</element><type>E</type><id></id><nameid>B10TEXTAREA_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10TEXTAREA]]></value><valuecode><![CDATA[="rasdTxLabB10TEXTAREA"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>78</elementid><pelementid>77</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B10TEXTAREA_LAB</id><nameid>B10TEXTAREA_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includ';
 v_vc(109) := 'evis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10TEXTAREA_LAB]]></value><valuecode><![CDATA[="B10TEXTAREA_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>79</elementid><pelementid>36</pelementid><orderby>22</orderby><element>TR_</element><type>B</type><id></id><nameid>B10_BLOCK22</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop>';
 v_vc(110) := '</endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>80</elementid><pelementid>37</pelementid><orderby>20</orderby><element>TX_</element><type>E</type><id></id><nameid>B10TEXTAREA_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10TEXTAREA rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10TEXTAREA rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10TEXTAREA_NAME]]></value><valuecode><![CDATA[="rasdTxB10TEXTAREA_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rfo';
 v_vc(111) := 'rm><rid></rid></attribute></attributes></element><element><elementid>81</elementid><pelementid>80</pelementid><orderby>1</orderby><element>TEXTAREA_</element><type>D</type><id>B10TEXTAREA</id><nameid>B10TEXTAREA</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTextarea ]]></value><valuecode><![CDATA[="rasdTextarea "]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10TEXTAREA_NAME_RASD]]></value><valuecode><![CDATA[="B10TEXTAREA_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10TEXTAREA_NAME]]></value><valuecode><![CDATA[="B10TEXTAREA_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><val';
 v_vc(112) := 'ue></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</textarea]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<textarea]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B10TEXTAREA_VALUE]]></value><valuecode><![CDATA[''||B10TEXTAREA(1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>82</elementid><pelementid>79</pelementid><orderby>581749</orderby><element>TX_</element><type>E</type><id></id><nameid>B10SUBMIT_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2';
 v_vc(113) := '</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10SUBMIT]]></value><valuecode><![CDATA[="rasdTxLabB10SUBMIT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>83</elementid><pelementid>82</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B10SUBMIT_LAB</id><nameid>B10SUBMIT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10SUB';
 v_vc(114) := 'MIT_LAB]]></value><valuecode><![CDATA[="B10SUBMIT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>84</elementid><pelementid>36</pelementid><orderby>581752</orderby><element>TR_</element><type>B</type><id></id><nameid>B10_BLOCK581752</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></el';
 v_vc(115) := 'ement><element><elementid>85</elementid><pelementid>79</pelementid><orderby>581750</orderby><element>TX_</element><type>E</type><id></id><nameid>B10SUBMIT_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10SUBMIT rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10SUBMIT rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10SUBMIT_NAME]]></value><valuecode><![CDATA[="rasdTxB10SUBMIT_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>86</elementid><pelementid>85</pelementid><orderby>1</orderby><element>INPUT_SUBMIT</element><type>D</type><id>B10SUBMIT</id><nameid>B10SUBMIT</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[=';
 v_vc(116) := '"rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10SUBMIT_NAME_RASD]]></value><valuecode><![CDATA[="B10SUBMIT_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10SUBMIT_NAME]]></value><valuecode><![CDATA[="B10SUBMIT_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></sou';
 v_vc(117) := 'rce><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10SUBMIT_VALUE]]></value><valuecode><![CDATA[="''||B10SUBMIT(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>87</elementid><pelementid>42</pelementid><orderby>40</orderby><element>TX_</element><type>E</type><id></id><nameid>B1OWNER_ID_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB1OWNER_ID rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB1OWNER_ID rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB1OWNER_ID_NAME]]></value><valuecode><![CDATA[="rasdTxB1OWNER_ID_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid><';
 v_vc(118) := '/attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>88</elementid><pelementid>87</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B1OWNER_ID</id><nameid>B1OWNER_ID</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1OWNER_ID_NAME_RASD]]></value><valuecode><![CDATA[="B1OWNER_ID_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><en';
 v_vc(119) := 'dloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![';
 v_vc(120) := 'CDATA[B1OWNER_ID_VALUE]]></value><valuecode><![CDATA[''||B1OWNER_ID(iB1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>89</elementid><pelementid>42</pelementid><orderby>60</orderby><element>TX_</element><type>E</type><id></id><nameid>B1DML_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB1DML rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB1DML rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB1DML_NAME]]></value><valuecode><![CDATA[="rasdTxB1DML_''||iB1||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>90</elementid><pelementid>89</pelementid><orderby>1</orderby><element>PLSQL_</element><type>D</type><id>B1DML</id><nameid>B1DML</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn><';
 v_vc(121) := '/hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B1DML_NAME_RASD]]></value><valuecode><![CDATA[="B1DML_''||iB1||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% B1DML_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''''SELECT \n'');

for r_1_ in (

select * from all_tab_columns x where x.owner = B1OWNER(iB1) and x.table_name =  B1ID(iB1)

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p('' ''||r_1_.column_name||''\n'');

  else

htp.p('',''||r_1_.column_name||''\n'');

  end if;



end loop;



htp.p(''FROM ''||B1OWNER(iB1)||''.''||B1ID(iB1)||'' \nWHERE 1=2 \nORDER BY 1'''') ;">S</A>'');





htp.p(''<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''''INSERT INTO ''||B1OWNER(iB1)||''.''||B1ID(iB1)||'' \n'');

htp.p(''(\n'');

for r_1_ in (

select * from all_tab_columns x where x.owner = B1OWNER(iB1) and x.table_name =  B1ID(iB1)

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p('' ''||r_1_.column_name||''\n'');

  else

htp.p('',''||r_1_.column_name||''\n'');

  end if;

end loop;

htp.p('') VALUES (\n'');

for r_1_ in (

select * from all_tab_columns x where x.owner = B1OWNER(iB1) and x.table_n';
 v_vc(122) := 'ame =  B1ID(iB1)

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p('' ''||r_1_.column_name||''\n'');

  else

htp.p('',''||r_1_.column_name||''\n'');

  end if;

end loop;

htp.p('')\n'''') ;">I</A>'');





htp.p(''<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''''UPDATE ''||B1OWNER(iB1)||''.''||B1ID(iB1)||'' SET\n'');

for r_1_ in (

select * from all_tab_columns x where x.owner = B1OWNER(iB1) and x.table_name =  B1ID(iB1)

order by x.column_id

) loop

  if r_1_.column_id = 1 then

htp.p('' ''||r_1_.column_name||'' = ''||r_1_.column_name||''\n'');

  else

htp.p('',''||r_1_.column_name||'' =''||r_1_.column_name||''\n'');

  end if;



end loop;



htp.p(''WHERE 1=2 '''') ;">U</A>'');





htp.p(''<A href="javascript: window.rasd_CMEditorB10TEXTAREA_1_RASD.setValue(''''DELETE ''||B1OWNER(iB1)||''.''||B1ID(iB1)||'' \n'');

htp.p(''WHERE 1=2 '''') ;">D</A>'');



htp.p(''</BR>'');
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>91</elementid><pelementid>53</pelementid><orderby>1999</orderby><element>TX_</element><type>E</type><id></id><nameid>B30PLSQL_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB30]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB30"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB30PLSQL]]></value><valuecode><![CDATA[="rasdTxLabB30PLSQL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![';
 v_vc(123) := 'CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>92</elementid><pelementid>91</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B30PLSQL_LAB</id><nameid>B30PLSQL_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30PLSQL_LAB]]></value><valuecode><![CDATA[="B30PLSQL_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textco';
 v_vc(124) := 'de><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>93</elementid><pelementid>52</pelementid><orderby>2002</orderby><element>TR_</element><type>B</type><id></id><nameid>B30_BLOCK2002</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>94</elementid><pelementid>53</pelementid><orderby>2000</orderby><element>TX_</element><type>E</type><id></id><nameid>B30PLSQL_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB30PLSQL rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB30PLSQL rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB30PLSQL_NAME]]></value><valuecode><![CDATA[="rasdTxB30PLSQL_1';
 v_vc(125) := '"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>95</elementid><pelementid>94</pelementid><orderby>1</orderby><element>PLSQL_</element><type>D</type><id>B30PLSQL</id><nameid>B30PLSQL</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30PLSQL_NAME_RASD]]></value><valuecode><![CDATA[="B30PLSQL_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attr';
 v_vc(126) := 'ibute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% B30PLSQL_VALUE %>]]></value><valuecode><![CDATA['');  if length(B10TEXTAREA(1)) > 0 and error is null then

/*

htp.p(''<div id="sqlresults_form" title="Results">

<table border="0" id="B30_TABLE">

<tr>

<td>*/

htp.p(''<PLSQL ID="B30RESULT_1" CLASS="FONT">'');



     if instr(upper(b10textarea(1)), ''SELECT'') > 0 then



         execute immediate ''begin  ''||user||''.EXECUTESQL.output(:sql,:lang); end;''

         using in b10textarea(1), in lang

         ;



/*

        declare

          cid1 pls_integer;

        begin

          cid1 := owa_util.bind_variables(b10textarea(1));



          htp.p(''<div class="sqlresults"><table>'');

          owa_util.cellsprint(cid1, 100);

          htp.p(''</table></div>'');



          b30sporocilo(1) := ''Only first 100 rows are shown.'';

       end;

*/

    end if;

      htp.prn(''</PLSQL>'');

/*	  </td></tr><tr>

<td></br><FONT ID="B30SPOROCILO_1" CLASS="FONT">'' ||

              B30SPOROCILO(1) || ''</FONT></td>

</tr></table>

</div>'');*/



end if;
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>96</elementid><pelementid>93</pelementid><orderby>2001</orderby><element>TX_</element><type>E</type><id></id><nameid>B30SPOROCILO_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB30]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB30"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB30SPOROCILO]]></value><valuecode><![CDATA[="rasdTxLabB30SPOROCILO"]]></valuecode><forloop></forloop><endloop></endloop><';
 v_vc(127) := 'source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>97</elementid><pelementid>96</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B30SPOROCILO_LAB</id><nameid>B30SPOROCILO_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30SPOROCILO_LAB]]></value><valuecode><![CDATA[="B30SPOROCILO_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>';
 v_vc(128) := '1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>98</elementid><pelementid>52</pelementid><orderby>2004</orderby><element>TR_</element><type>B</type><id></id><nameid>B30_BLOCK2004</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>99</elementid><pelementid>93</pelementid><orderby>2002</orderby><element>TX_</element><type>E</type><id></id><nameid>B30SPOROCILO_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB30SPOROCILO rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB30SPOROCILO rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid';
 v_vc(129) := '></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB30SPOROCILO_NAME]]></value><valuecode><![CDATA[="rasdTxB30SPOROCILO_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>100</elementid><pelementid>99</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B30SPOROCILO</id><nameid>B30SPOROCILO</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></';
 v_vc(130) := 'text><name><![CDATA[id]]></name><value><![CDATA[B30SPOROCILO_NAME_RASD]]></value><valuecode><![CDATA[="B30SPOROCILO_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform><';
 v_vc(131) := '/rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B30SPOROCILO_VALUE]]></value><valuecode><![CDATA[''||B30SPOROCILO(1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_RASDC2_SQLCLIENT_v.1.1.20240228091007.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end RASDC2_SQLCLIENT;

/
